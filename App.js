import React from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { Provider } from 'react-redux'
import { store } from './src/Redux/store'

import Appstack from './src/Routes/Appstack'
import globalStyle from './src/Util/globalStyle'


const App = () => {
  const linking = {
    prefixes: [
      "https://remindme-web.herokuapp.com/", 
    ],
    config: {
      screens: {
        OnboardingStack : {
          screens : {
            Verify : 'verification/:token',
            Forgot : {path: 'reset/:token'}
          }
        } 
      }
    },
  };

  return (
    <Provider store={store}>
      <NavigationContainer linking={linking} fallback={<ActivityIndicator size="large" color="blue" style={{flex:1, backgroundColor:globalStyle.primaryColor}}/>}>
        <Appstack />
      </NavigationContainer>
    </Provider>
  )
}

export default App
