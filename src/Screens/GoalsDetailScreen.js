import React, { useEffect, useState } from 'react'
import { ActivityIndicator, Button, FlatList, Image, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import {HeaderWithEdit} from '../Components/Header'
import CircularProgress from 'react-native-circular-progress-indicator';
import { AddProgressModal, AlertModal } from '../Components/Modal';
import { getRandomColor } from '../Util/RandomColor';
import { useDispatch, useSelector } from 'react-redux';
import globalStyle from '../Util/globalStyle';



const GoalsDetailScreen = (props) => {
    const dispatch = useDispatch()
    const goalsdetailData = useSelector(state => state.goals.goalsDataDetail)
    const loadingDetail = useSelector(state => state.goals.isLoadingDetail)
    const modalDetailState = useSelector(state => state.goals.modalStateDetail)

    const [modalState, setModalState] = useState(false)
    const [completeModal, setCompleteModal] = useState(false)
    console.log("==> goalsDetailData", goalsdetailData)

    const [progressInput, setProgressInput] = useState()
    console.log("progress", progressInput)
    
    const getColor = getRandomColor()
    
    const handleSaveProgress = () => {
        if(progressInput > goalsdetailData.target) {
            alert('progress value is bigger than target value')
        } else if(progressInput < 1) {
            alert('Input should be more than 0')
        } else {
            let newProgress = {
                progress : parseInt(progressInput),
                color: getRandomColor()
            }
            dispatch({
                type:"POST_GOAL_PROGRESS", 
                progressPayload : {
                    progressData: newProgress, 
                    goalsId: goalsdetailData.id
                }})
        }
    }
    // const targetVal = (100/200) * 100
    

    // useEffect(() => {
        
    // }, [progressBar])

    // const DATA = [
    //     {
    //       id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    //       title: 'First Item',
    //       colorBg: getRandomColor()
    //     },
    //     {
    //       id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    //       title: 'Second Item',
    //       colorBg: getRandomColor()
    //     },
    //     {
    //       id: '58694a0f-3da1-471f-bd96-145571e29d72',
    //       title: 'Third Item',
    //       colorBg: getRandomColor()
    //     },
    //   ];



    return (
        <View style={styles.backgroundBase}>
            <HeaderWithEdit 
                title={"GoalsDetail"}
                onPressBack={() => props.navigation.navigate('Home')}
                onPressEdit={() => props.navigation.navigate('GoalsScreen', {goalsData: goalsdetailData})}
            />
                {/* circular progress bar */}
            {loadingDetail ? <ActivityIndicator size="large" color="blue" />
            :<View style={{flex:1, width:'100%', alignItems:'center'}}>
                <View style={styles.progressBarContainer}>
                    <CircularProgress
                        value={goalsdetailData.current_percent}
                        radius={100}
                        duration={1000}
                        maxValue={100}
                        valueSuffix={'%'}
                        onAnimationComplete={() => {
                            goalsdetailData.current === goalsdetailData.target ? setCompleteModal(true) : null
                        }}
                        showProgressValue={false}
                        inActiveStrokeColor={'#F1F4FA'}
                        activeStrokeColor={goalsdetailData.color === '#FFBCC2' ? '#FF8888' //pink
                                            : goalsdetailData.color === '#FCF3A1' ? '#F6E64C' //lemon
                                            : goalsdetailData.color === '#CCF0D7' ? '#34A69A' //green
                                            : goalsdetailData.color === '#FF8888'? '#FF586A' //red
                                            : goalsdetailData.color === '#D1CDFA' ? '#A258FF' //purple
                                            : null
                                            }
                    />
                    <View style={{...styles.customText, backgroundColor:goalsdetailData.color}}>
                        <Text style={{fontSize:26, fontFamily:'Poppins-Medium'}}>{goalsdetailData.current_percent}%</Text>
                        <Text style={{fontSize:18, fontFamily:'Poppins-Thin'}}>{goalsdetailData.target} {goalsdetailData.target_type}</Text>
                        <Text style={{fontSize:14, fontFamily:'Poppins-Thin'}}>{goalsdetailData.current} | -{goalsdetailData.target - goalsdetailData.current}</Text>
                    </View>
                </View>
                
                <TouchableOpacity 
                    style={{
                        flexDirection: 'row', 
                        backgroundColor: goalsdetailData.color,
                        borderRadius: 20, 
                        padding:10,
                        marginVertical:20
                    }}
                    onPress={() => setModalState(true)}
                    >
                    <Image source={require('../Assets/Icons/AddProgressIcon.png')} style={{width:20, height:20}} />
                    <Text>Add Progress</Text>
                </TouchableOpacity>
                
                
                <View style={{ width:'100%', flex:1}}>
                    <FlatList
                        data={goalsdetailData.progresses}
                        keyExtractor={((item, index) => index)}
                        renderItem={({item}) => 
                            
                            (<View style={{
                                flexDirection:'row', 
                                margin:10, 
                                borderRadius:20,
                                backgroundColor: item.color,
                                alignSelf:'center',
                                justifyContent:'center',
                                width:'100%',
                                padding:10,
                                
                                }}>
                                <Image source={require('../Assets/Icons/VectorIcon.png')} style={{width:20, height:20,marginRight:20}}/>
                                <Text style={{fontFamily:'Poppins-Regular', color:'black',}}>{item.progress} {goalsdetailData.target_type}</Text>
                            </View>)
                        
                        }


                        ListHeaderComponent={() => 
                            <Text style={{fontFamily:'Poppins-Regular', fontSize:16}}>
                                History
                            </Text>
                        }
                        ListEmptyComponent={() => 
                            <Text style={{fontFamily:'Poppins-Regular', fontSize:16, alignSelf:"center", justifyContent:'center'}}>
                                No Progression yet
                            </Text>
                        }
                    />
                </View>

                <AddProgressModal 
                    modalVisible={modalState}
                    onRequestClose={() => setModalState(false)}
                    onBackdropClick={() => setModalState(false)}
                    onPressSave={() => {
                        handleSaveProgress()
                        setModalState(false)
                    }}
                    typeValue={goalsdetailData.target_type}
                    onChangeValue={(text) => setProgressInput(text)}
                    inputValue={progressInput}
                    maxLength={4}
                />
            </View>}
            
            <AlertModal 
                alertModalVisible={modalDetailState}
                cardImage={require('../Assets/Icons/RoundCrossIcon.png')}
                cardText="Sorry we couldn't add your progress at the moment"
                buttonTitle='Ok'
                onPressCardButton={() => {
                    dispatch({type: 'POST_GOALS_PROGRESS_FAILED_OK'})
                    props.navigation.navigate('Home')
                }}
            />

            <AlertModal 
                alertModalVisible={completeModal}
                cardImage={require('../Assets/Icons/CelebrateIcon.png')}
                cardText={"Congratulation"+ "\n" + "You have completed your goal"}
                buttonTitle='Ok'
                onPressCardButton={() => {
                    setCompleteModal(false)
                }}
            />






        </View>
    )
}

export default GoalsDetailScreen

const styles = StyleSheet.create({
    backgroundBase : {
        flex:1,
        backgroundColor: globalStyle.primaryColor,
        alignItems:'center',
        paddingHorizontal:20,
    },
    progressBarContainer : {
        backgroundColor:globalStyle.primaryColor, 
        borderRadius:100,
        justifyContent:'center', 
        width:200, 
        height:200,
        marginVertical:20
    },
    customText : {
        position:'absolute', 
        alignSelf:'center', 
        alignItems:"center",
        justifyContent:'center',
        backgroundColor:'lightblue', 
        width: 175,
        height:175,
        borderRadius:100,
    }
})
