import React, { useEffect, useState } from 'react'
import { Dimensions, Image, Keyboard, KeyboardAvoidingView, Linking, Modal, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import PasswordField from '../Components/PasswordField'
import { PrimaryButtonLarge } from '../Components/PrimaryButton'
import globalStyle from '../Util/globalStyle'
import Icon from 'react-native-vector-icons/AntDesign'
import { useDispatch, useSelector } from 'react-redux'
import { BlurView } from '@react-native-community/blur'
import { AlertCard, LoadingCard } from '../Components/Card'

const ScreenHeight = Dimensions.get('screen').height
const ScreenWidth = Dimensions.get('screen').width

const passwordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/

const ForgotScreen = (props) => {
    const dispatch = useDispatch()
    const modalState = useSelector(state => state.auth.changeModalState)
    const loading = useSelector(state => state.auth.loadingChangePassword)
    const message = useSelector(state => state.auth.message)

    const [newPasswordPlaceHolder, setNewPasswordPlaceHolder] = useState('New Password')
    const [confirmPasswordPlaceHolder, setconfirmPasswordPlaceHolder] = useState('Confirm Password')
    const [newPassword, setNewPassword] = useState('')
    const [confirmPassword, setconfirmPassword] = useState('')
    const [urlStore, setUrlStore] = useState('')
    console.log('token State', urlStore)

    const handleChangePassword = () => {
        console.log('clicked')
        if(urlStore !== '' && newPassword.match(passwordFormat) && confirmPassword.match(newPassword)) {
            let newPasswordData = {
                new_password: newPassword,
                confirm_password: confirmPassword,
            }
            dispatch({type: 'CHANGE_PASSWORD_AUTH', dataPayload: {newPasswordData : newPasswordData, url: urlStore}})
        } else if (!newPassword.match(passwordFormat)) {
            alert('Password must contain one Capital letter, one number, and one special case and minimum 8 character')
        } else if (confirmPassword.match(newPassword)) {
            alert("Password doesn't match")
        }
    }

    useEffect(async() => {
        const getUrlAsync = async () => {
            const initialUrl = await Linking.getInitialURL();
            const token = initialUrl?.split('/')[4]
            console.log('token get initial2',initialUrl)
            // console.log('url', initialUrl)
            if (token) {
                setUrlStore(token)
            }
            };
        

        getUrlAsync();
    }, []);

    useEffect(async() => {
        await Linking.addEventListener('url', e => {
            try {
                const token = e?.url?.split('/')[4]
                console.log('token add event listener', e)
                if(token) {
                    setUrlStore(token)
                }
            }catch(error) {
                console.log(error)
            }
            });
    }, [])

    if(urlStore?.length === 0) {
        return (
        <View style={{flex:1, alignItems:'center', backgroundColor:globalStyle.primaryColor}}>
            <View style={{width:350, height:250, marginVertical:40}}>
                <Image source={require('../Assets/Images/Register-4.png')} style={{width:'100%', height:'100%'}}/>
            </View>
            <Text style={{fontSize:24, fontFamily:"Poppins-Regular"}}>Please check your email first</Text>
        </View>)
    }
    return (
       <ScrollView>
        <View style={{
            flex:1,
            alignItems:'center', 
            backgroundColor:globalStyle.primaryColor,
            padding:20
            }}>
            <Text style={{...styles.textInfo, fontSize:20}}>Forgot Password</Text>
            <View style={{width:300, height:250}}>
                <Image source={require('../Assets/Images/Register-5.png')} style={{width:300, height:200}} />
            </View>
            

            <View style={{width:'100%', padding:20, height:250, alignItems:'center'}}>
                <View style={{height:70, alignItems:'center'}}>
                    <PasswordField 
                        title={newPasswordPlaceHolder}
                        onFocus={() => setNewPasswordPlaceHolder('')}
                        onBlur={() => setNewPasswordPlaceHolder('New Password')}
                        value={newPassword}
                        onChangeText={(text) => setNewPassword(text)}
                        onSubmitEditing={() => Keyboard.dismiss()}
                    />
                    {newPassword == ''? null : newPassword.match(passwordFormat) 
                    ? <Text style={{fontFamily:'Poppins-Regular', color:'green'}}>Good Password</Text>
                    : <Text style={{fontFamily:'Poppins-Regular', color:'red'}}>Bad Password</Text>}
                </View>
                <View style={{height:70, alignItems:'center'}}>
                <PasswordField 
                    title={confirmPasswordPlaceHolder}
                    onFocus={() => setconfirmPasswordPlaceHolder('')}
                    onBlur={() => setconfirmPasswordPlaceHolder('Confirm Password')}
                    value={confirmPassword}
                    onChangeText={(text) => setconfirmPassword(text)}
                    onSubmitEditing={() => Keyboard.dismiss()}
                />
                {confirmPassword == ''? null : confirmPassword.match(newPassword)
                ? <Text style={{fontFamily:'Poppins-Regular', color:'green'}}>Password Match</Text>
                : <Text style={{fontFamily:'Poppins-Regular', color:'red'}}>Password didn't match</Text> 
                }
                </View>
                <PrimaryButtonLarge 
                    title={'Change Password'}
                    onPress={() => handleChangePassword()}
                />
            </View>

            <View>
                <Text style={styles.textInfo}><Icon name="infocirlce" size={16} /> Password must contain minimum length 8</Text>
                <Text style={styles.textInfo}><Icon name="infocirlce" size={16} /> Password must contain at least one uppercase letter</Text>
                <Text style={styles.textInfo}><Icon name="infocirlce" size={16} /> Password must contain at least one number</Text>
                <Text style={styles.textInfo}><Icon name="infocirlce" size={16} /> Password must contain at least one special character </Text>
            </View>
            
        </View>

        <Modal 
            animationType="fade"
            visible={modalState}
            transparent={true}
            >
                <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

                    <TouchableWithoutFeedback
                        onPress={{}}
                    >
                        <View style={styles.modalOverlay}/>
                    </TouchableWithoutFeedback>
                    
                    <View style={{position:'absolute', width:300, height:250, left:'10%', bottom:'30%'}}>
                    {loading 
                        ? <LoadingCard />
                        : message == 'success' 
                        ? <AlertCard 
                            cardTitle={true}
                            cardTitleText='Change Password Success'
                            cardImage={require('../Assets/Icons/RoundCheckIcon.png')}
                            cardText={`We have successfully change password`}
                            buttonTitle="Ok"
                            onPressCardButton={() => {
                                dispatch({type: 'CHANGE_PASSWORD_AUTH_OK'})
                                props.navigation.navigate('Login')
                            }}
                        />
                        : <AlertCard 
                            cardTitle={true}
                            cardTitleText={`Change Password Failed`}
                            cardImage={require('../Assets/Icons/RoundCrossIcon.png')}
                            cardText={`Sorry, we couldn't change your password right now`}
                            buttonTitle="Ok"
                            onPressCardButton={() => {
                                dispatch({type: 'CHANGE_PASSWORD_AUTH_FAILED'})
                            }}
                        />
                        }
                        
                    </View>

            </Modal>
        </ScrollView>
    )
}

export default ForgotScreen

const styles = StyleSheet.create({
    textInfo: {
        fontFamily:'Poppins-Regular',
        color:globalStyle.TCdark,

    }
})
