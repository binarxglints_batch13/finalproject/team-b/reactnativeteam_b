import React, { useState } from 'react'
import { ActivityIndicator, Dimensions, FlatList, Image, ScrollView, SectionList, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { NoteCard } from '../Components/Card'
import { HeaderWithText } from '../Components/Header'
import moment from 'moment'
import AddButton from '../Components/AddButton'
import globalStyle from '../Util/globalStyle'

const AllNotesScreen = (props) => {
    const dispatch = useDispatch()
    
    const notesData = useSelector(state => state.notes.notesData)
    const isLoadingData = useSelector(state => state.notes.isLoadingData)
    // console.log('INI DI SCREEN NOTES',notesData)
    const notesDataPinned = notesData?.filter((item) => item.pinned === true)
    const notesDataUnpinned = notesData?.filter((item) => item.pinned === false)
    // console.log("pinned",notesDataPinned)
    // console.log("unpinned", notesDataUnpinned)
    const DATA = [
        {
            title : <Text style={{fontFamily:'Poppins-Regular', fontSize:20}}>Pinned</Text>,
            data : [{
                list : notesDataPinned
            }]
        },
        {
            title : <Image source={require('../Assets/Images/borderline.png')} style={{width:'100%'}}/>,
            data : [{
                list : notesDataUnpinned
            }]
        },
    ]
    
    const navigateNotesDetails = (item) => {
        dispatch({type: 'GET_NOTES_DETAIL', notesId: item.id})
        dispatch({type:'READ_STATE'})
        props.navigation.navigate('NotesScreen', {notesDetailData: item} )
    }


    return (
        <View style={styles.backgroundBase}>
            <HeaderWithText
                title="My Notes"
                onPressBack={() => props.navigation.navigate('Home')}
            />
            {/* yang dipake! */}    
            {isLoadingData ? <ActivityIndicator size='large' color="blue" />
            :<SectionList 
                sections={DATA}
                renderItem={({item}) => {
                    return(
                        <FlatList 
                            data={item.list}
                            keyExtractor={(item,index) => index}
                            numColumns={2}
                            renderItem={({item}) => {
                                return(
                                    <NoteCard 
                                        title={item.title}
                                        pinState={item.pinned}
                                        time={item.dateNote}
                                        body={item.body}
                                        notesColor={item.color}
                                        onPressNote={() =>  {
                                            navigateNotesDetails(item)
                                            
                                        }}
                                    />
                                )
                            }}
                        />
                    )
                }}
                keyExtractor={(item,index) => index}
                renderSectionHeader={({section}) => {
                    return(
                        <View>
                            {section.title}
                        </View>
                    )
                    }}
                ListEmptyComponent={() => (<Text style={{alignSelf:'center'}}>No data</Text>)}
                style={{width:'100%'}}
                />
            }
            <AddButton
                buttonStyle={{
                    right: 20,
                    bottom: 20,
                    tintColor: globalStyle.secondaryColorDark,
                }}
            />


            
        </View>
        
    )
}

export default AllNotesScreen

const styles = StyleSheet.create({
    backgroundBase : {
        flex:1,
        backgroundColor: '#F1F4FA',
        alignItems:'center',
        paddingHorizontal:10
    },
})
