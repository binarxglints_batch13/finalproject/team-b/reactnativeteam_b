import React, { useEffect, useRef, useState } from 'react'
import { Dimensions, Image, Alert, StyleSheet, Text, View } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import { OutlineButtonLarge } from '../Components/OutlineButton'
import { PrimaryButtonLarge } from '../Components/PrimaryButton'
import globalStyle from '../Util/globalStyle'
import messaging from '@react-native-firebase/messaging';
import { saveFCMToken } from '../Util/authAsyncStorage'


const ScreenWidth = Dimensions.get('screen').width
const WindowWidth = Dimensions.get('window').width

const LandingPage = (props) => {
    const isCarousel = useRef(null)
    const [carIndex, setCarIndex] = useState(0)

    useEffect(async () => {
        // Register background handler
        const token = await messaging().getToken()
        // console.log("token", token)
        saveFCMToken(token)
        messaging().setBackgroundMessageHandler(async remoteMessage => {
          console.log('Message handled in the background!', remoteMessage);
        });
    
        const unsubscribe = messaging().onMessage(async remoteMessage => {
          Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
          
        });
    
        return unsubscribe;
      }, []);
    
    const _CAROUSELDATA = [
        {
            title:'Organize Your Life',
            subTitle: 'Set your life to be more scheduled',
            image: require('../Assets/Images/Splash-1.png')
        },
        {
            title:'Achieve Your Goal',
            subTitle: 'Reach your goal faster',
            image: require('../Assets/Images/Splash-2.png')
        },
        {
            title:'No More Forget',
            subTitle: 'Get a reminder to let you know what you want to do',
            image: require('../Assets/Images/Splash-3.png')
        },
    ]

    const carouselItem = ({item}) => {
        return(
            <View style={{width:'100%',}}>
                <View style={{padding:20}}>
                    <Text style={{fontFamily:'Poppins-Medium', fontSize:24}}>{item.title}</Text>
                    <Text style={{fontFamily:'Poppins-Thin', fontSize:16}}>{item.subTitle}</Text>
                </View>
                <View style={{alignItems:'center'}}>
                    <Image source={item.image} style={{width:300 , height:300,}} />
                </View>
            </View>   
        )
    }

    return (
        <View style={styles.backgroundBase}>
            <View style={{width:ScreenWidth,}}>
                <Carousel 
                    ref={isCarousel}
                    data={_CAROUSELDATA}
                    renderItem={carouselItem}
                    itemWidth={WindowWidth}
                    sliderWidth={ScreenWidth}
                    onSnapToItem={(index) => setCarIndex(index)}
                    // loop={true}
                    // autoplay={true}
                    // autoplayDelay={3000}
                    // autoplayInterval={3000}
                />
                <Pagination
                dotsLength={_CAROUSELDATA.length}
                carouselRef={isCarousel}
                activeDotIndex={carIndex}
                containerStyle={{ backgroundColor: '#F1F4FA' }}
                dotStyle={{
                    width: 15,
                    height: 15,
                    borderRadius: 50,
                    backgroundColor: globalStyle.secondaryColorDark
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                    width: 15,
                    height: 15,
                    backgroundColor: '#C4C4C4'
                    
                }}
                inactiveDotOpacity={1}
                inactiveDotScale={1}
                />
            </View>
            
            <PrimaryButtonLarge 
                title="Get Started"
                onPress={() => props.navigation.navigate('Register')}
            />
            <OutlineButtonLarge 
                title="I Have An Account"
                textStyle={{
                    fontFamily:'Poppins-Medium',
                }}
                onPress={() => props.navigation.navigate('Login')}
            />
            

        </View>
    )
}

export default LandingPage

const styles = StyleSheet.create({
    backgroundBase : {
        flex:1,
        backgroundColor: globalStyle.primaryColor,
        alignItems:'center',
        paddingHorizontal:20
    },
})
