import React, { useEffect, useState } from 'react'
import { ActivityIndicator, Dimensions, FlatList, Image, ScrollView, StyleSheet, Text, Touchable, TouchableOpacity, View } from 'react-native';
import { Calendar } from 'react-native-calendars';
import Svg, { Circle } from 'react-native-svg';
import { HeaderWithText } from '../Components/Header';
import moment from 'moment';
import { PrimaryButtonLarge } from '../Components/PrimaryButton';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'
import { useDispatch, useSelector } from 'react-redux';
import GoalsItem from '../Components/GoalsItem';
import TimelineItem from '../Components/TimelineItem';
import globalStyle from '../Util/globalStyle';
import AddButton from '../Components/AddButton';


const CalendarScreen = (props) => {
    const todayDate = moment().format('YYYY-MM-DD');
    const dispatch = useDispatch()
    const goalsData = useSelector(state => state.goals.goalsData)
    const notesData = useSelector(state => state.notes.notesData)
    const notesDataByDate = useSelector(state => state.notes.notesDatabyDate)
    const isLoadingNotes = useSelector(state => state.notes.isLoadingData)
    const isLoadingDataDate = useSelector(state => state.notes.isLoadingDataDate)
    

    const [dayState, setDayState] = useState(todayDate)
    

    // console.log("ini notes data",notesData)
    const dotedote =  () => {
        let newObject = notesData.reduce((prev, item) => ({
            ...prev, [moment.utc(item.dateNote).format('Y-MM-DD')]: {dots: []}
        }), {})
        for(let item of notesData) {
            let date = moment.utc(item.dateNote).format('Y-MM-DD') 
            if(newObject.hasOwnProperty(moment.utc(item.dateNote).format('Y-MM-DD'))) {
                newObject[`${date}`]['dots'].push({'key':item.id, 'color': item.color}) 
            }
        }
        return newObject
    }

    const navigateDetails = async (item) => {
        await dispatch({type:'GET_GOALS_DETAIL_BY_ID', goalsId: item.id})
        props.navigation.navigate('GoalsDetailScreen', {goalsId: item.id})
    }

    const navigateNotesDetails = (item) => {
        dispatch({type: 'GET_NOTES_DETAIL', notesId: item.id})
        dispatch({type:'READ_STATE'})
        props.navigation.navigate('NotesScreen', {notesDetailData: item} )
    }

    const renderGoalItem = ({item}) => {
        return(
            <GoalsItem 
                onPress={() => navigateDetails(item)}
                itemColor={item.color}
                itemName={item.name}
                percent={item.current_percent}
            />
        )
    }

    const renderTimeline = ({item}) => {
        return(
            <TimelineItem 
                title={item.title}
                color={item.color}
                body={item.body}
                dateNote={item.dateNote}
                onPress={() => navigateNotesDetails(item)}
            />
        )
    }

    
    useEffect(() => {
        dispatch({type:'GET_NOTES_BY_DATE', date: dayState})
    }, [dayState])

    return (
        <View style={styles.backgroundBase}>
            <HeaderWithText 
                title="Calendar"
                onPressBack={() => props.navigation.navigate('Home')}
            />

            <View style={{width:'100%', }}>
                <Calendar
                markedDates={typeof notesData == 'undefined' ? {} : dotedote()}
                hideExtraDays
                theme={{
                    backgroundColor:"#F1F4FA",
                    calendarBackground:'#F1F4FA',
                    textSectionTitleColor:'#342D50',
                    textDayFontFamily: 'Poppins-Regular',
                    textDayHeaderFontFamily: 'Poppins-Regular',
                    disabledArrowColor:'#B6C6E5',
                    textDayFontSize:14,
                    textDayHeaderFontSize:12,
                    'stylesheet.calendar.main' : {
                        week: {
                            flexDirection: 'row',
                            justifyContent: 'space-around'
                          },
                    }
                }}
                // renderArrow={(direction) => {
                //     direction === "left" 
                //     ? (<MaterialIcon name='keyboard-arrow-left' size={20}/>)
                //     : (<Image source={require('../Assets/Icons/BackArrow.png')} style={{width:10, height:16}}/>)
                // }}
                dayComponent={(props) => {
                    // console.log(props)
                    return (
                    <View>
                        <TouchableOpacity onPress={() => setDayState(props.date.dateString)} >
                            <View style={[{ alignItems:'center',width:20, height:30,}, dayState === props.date.dateString ? styles.selectedDate : null]}>
                                <Text style={{...styles.customDay, color : dayState === props.date.dateString ? "#F1F4FA" : "#342D50"}}>
                                {props.date.day}
                                </Text>
                                {props.marking 
                                ? props.marking.dots.length > 1 ? (
                                <View style={{flexDirection:'row', }}>
                                    <View style={{width:5,height:5,borderRadius:20, marginHorizontal:1 ,backgroundColor:props.marking.dots[0].color}}/> 
                                    <View style={{width:5,height:5,borderRadius:20, backgroundColor:props.marking.dots[1].color}}/>
                                </View> ) : (
                                <View style={{flexDirection:'row'}}>
                                    <View style={{width:5,height:5,borderRadius:20,backgroundColor:props.marking.dots[0].color}}/> 
                                </View> ) : null}
                            </View>
                        </TouchableOpacity>
                    </View>
                    );
                }}
                />

                <View style={{flexDirection:"row", justifyContent:'flex-end', paddingHorizontal: 20}}>
                    <View style={{flexDirection:"row", alignItems:'center', marginHorizontal:10}}>
                        <View style={{backgroundColor:'#625BAD', width:10, height:10, borderRadius:20, marginRight:5}}/>
                        <Text>Today</Text>
                    </View>
                    <View style={{flexDirection:"row", alignItems:'center'}}>
                        <View style={{backgroundColor:'#B6C6E5', width:10, height:10, borderRadius:20, marginRight:5}}/>
                        <Text>Selected Day</Text>
                    </View>
                </View>

                
                {/* <PrimaryButtonLarge title='test' onPress={() => dotedote()} /> */}
                <View style={{width:'100%', paddingHorizontal:10}}>
                    <Image source={require('../Assets/Images/borderline.png')} style={{width:'100%',}}/>
                </View>
                
                { isLoadingNotes 
                ? <ActivityIndicator size="large" color="blue" style={{flex:1, backgroundColor:globalStyle.primaryColor}}/>
                : goalsData && notesData 
                ?   <View style={{width:'100%'}}>
                        <View style={{flex:1,width:'100%', marginVertical:10}}>
                            <Text style={{fontFamily:'Poppins-Regular', marginVertical:5, paddingLeft:10}}>Daily Streak</Text>
                            <FlatList 
                                data={typeof goalsData == 'undefined' ? null : goalsData}
                                renderItem={renderGoalItem}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                extraData={goalsData}
                                ListEmptyComponent={() => {
                                    return(
                                        <View style={{width:Dimensions.get('screen').width, justifyContent:'center', alignItems:'center'}}>
                                            <Text>No Goals for today</Text>
                                        </View>
                                        )
                                }}
                            />
                        </View>
                        <View style={{ width:'100%', paddingHorizontal:10, height:'50%'}}>
                            <Text style={{fontFamily:'Poppins-Regular'}}>Today's Timeline</Text>
                            {isLoadingDataDate ? <ActivityIndicator size="large" color="blue" style={{flex:1, backgroundColor:globalStyle.primaryColor}}/>
                            :   <FlatList 
                                data={notesDataByDate.length == 0 ? null : notesDataByDate}
                                renderItem={renderTimeline}
                                extraData={notesDataByDate}
                                showsVerticalScrollIndicator={false}
                                ListEmptyComponent={() => {
                                    return(
                                        <View style={{
                                            width:Dimensions.get('screen').width, 
                                            justifyContent:'center', 
                                            alignItems:'center',
                                            height:100,
                                            }}>
                                            <Text>No timeline for today</Text>
                                        </View>
                                    )
                                }}
                            />
                            }
                        </View>
                    </View> 
                :   <View style={{ width:'100%', height:400}}>
                        <Image source={require('../Assets/Images/LogoWelcome.png')} style={{width:'100%', height:'80%'}}/>
                    </View>
                }
                
            </View>

            <AddButton
                buttonStyle={{
                    right: 20,
                    bottom: 20,
                    tintColor: globalStyle.secondaryColorDark,
                }}
            />
        </View>
    )
}

export default CalendarScreen

const styles = StyleSheet.create({
    backgroundBase : {
        flex:1,
        height:Dimensions.get('screen').height,
        backgroundColor: '#F1F4FA',
        alignItems:'center',
        paddingHorizontal:10,

    },
    customDay: {
        textAlign: 'center',
        fontFamily:'Poppins-Regular'
      },
    disabledText: {
        color: 'grey'
    },
    defaultText: {
        color: 'black'
      },
    selectedDate: { 
        borderRadius:20,
        backgroundColor:'#625BAD'
    }
})
