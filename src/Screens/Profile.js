import React, { useRef, useState } from 'react'
import { Dimensions, Image, Keyboard, KeyboardAvoidingView, Modal, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import Svg, { Circle, Ellipse, Path } from 'react-native-svg'
import { HeaderWithText } from '../Components/Header'
import {ProfileInputField} from '../Components/InputField'
import { PrimaryButtonMed } from '../Components/PrimaryButton'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { useDispatch, useSelector } from 'react-redux'
import { AlertModal, ChangePhotoModal, PasswordModal } from '../Components/Modal'
import { launchCameraLib, launchImageLib } from '../Util/ImagePickerFunc'
import { BlurView } from '@react-native-community/blur'
import { AlertCard, LoadingCard } from '../Components/Card'
import { removeToken } from '../Util/authAsyncStorage'
import globalStyle from '../Util/globalStyle'


const passwordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/

const Profile = (props) => {
    const dispatch = useDispatch()

    const userData = useSelector(state => state.user.userData)
    const loadingProfile = useSelector(state => state.user.isLoading)
    const message = useSelector(state => state.user.message)

    const [alertModal, setAlertModal] = useState(false)
    const [modalImage, setModalImage] = useState(false)
    const [logoutModal, setLogoutModal] = useState(false)
    const [modalChangePassword, setModalChangePassword] = useState(false)
    const [editState, setEditState] = useState(false)

    const [firstname, setFirstname] = useState(userData.firstname)
    const [lastname, setLastname] = useState(userData.lastname)
    const [username, setUsername] = useState(userData.username)
    const [email, setEmail] = useState(userData.email)
    
    const [imageStore, setImageStore] = useState(null)
    const [image, setImage] = useState(userData.image)

    const [oldPassword, setOldPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const passwordRef = useRef()
    const newPasswordRef = useRef()

    const handleImage = (e) => {
        setImageStore(e)
        setImage(e.uri)
    }

    const handleFromLibrary = async() => {
        let e = await launchImageLib(handleImage)
        setModalImage(false)
    }

    const handleFromCamera = async() => {
        let e = await launchCameraLib(handleImage)
        setModalImage(false)
    }
    const handleRemoveImage = () => {
        setImage("https://res.cloudinary.com/remindme/image/upload/v1629817403/Users/user_dev8vy.png")
        setModalImage(false)
    }

    const resetData = () => {
        setFirstname(userData.firstname)
        setLastname(userData.lastname)
        setUsername(userData.username)
        setEmail(userData.email)
        setImage(userData.image)
    }

    const handleUpdateProfile = () => {
        if(firstname && lastname && username) {
            let dataForm = new FormData()
            dataForm.append('firstname', firstname)
            dataForm.append('lastname', lastname)
            dataForm.append('username', username)
            dataForm.append('email', email)
            dataForm.append('password', oldPassword)
            dataForm.append('new_password', newPassword)
            dataForm.append('confirm_password', confirmPassword)
            if (imageStore !== null) {
                dataForm.append('image', { 
                    name: imageStore.fileName,
                    type: imageStore.type,
                    uri: imageStore.uri
                })
            }
            dispatch({type:'UPDATE_PROFILE', newProfile: dataForm})
            setAlertModal(true)
        } else {
            alert('firstname/lastname/username cannot be empty')
        } 
    }

    const handleChangePassword = () => {
        if(newPassword.match(passwordFormat) && confirmPassword.match(newPassword)) {
            let dataForm = new FormData()
            dataForm.append('firstname', firstname)
            dataForm.append('lastname', lastname)
            dataForm.append('username', username)
            dataForm.append('email', email)
            dataForm.append('password', oldPassword)
            dataForm.append('new_password', newPassword)
            dataForm.append('confirm_password', confirmPassword)
            // let newDataForm ={
                
            //     new_password: newPassword,
            //     confirm_password: confirmPassword
            // }
            dispatch({type: 'UPDATE_PROFILE', newProfile : dataForm})
            setOldPassword('')
            setNewPassword('')
            setConfirmPassword('')
            setModalChangePassword(false)
            setAlertModal(true)
        } else if (!newPassword.match(passwordFormat)) {
            alert('Password must contain one Capital letter, one number, and one special case and minimum 8 character')
        } else if (confirmPassword.match(newPassword)) {
            alert("Password doesn't match")
        }
    }

    const handleLogout = () => {
        dispatch({type:'RESET_STATE'})
        dispatch({type: 'LOGOUT'})
    }


    return (
        <KeyboardAvoidingView behavior='position' style={{flex:1}}>
        <View style={styles.backgroundBase}>
            <HeaderWithText 
                    title={editState ? "Edit Profile" : "My Profile"}
                    onPressBack={() => props.navigation.navigate('Dashboard')}
                    headerContainerStyle={{
                        paddingLeft: 10
                    }}
                />
            {/* <View style={styles.oval}/> */}
           <Svg width={400} height={300} style={{position:'absolute', zIndex:-1,}}>  
                <Ellipse
                    cx="200"
                    cy="0"
                    rx="300"
                    ry="150"
                    strokeWidth="1"
                    fill="#B6C6E5"
                />
            </Svg>
            <View style={{alignItems:'center',justifyContent:'space-between', marginTop:20 ,marginBottom:10, }}>
                <TouchableOpacity disabled={editState ? false : true} onPress={() => setModalImage(true)}>
                    <View style={{width:120, height:50, position:'absolute', zIndex:2, bottom:0, opacity:editState ? 1 : 0}}>
                        <Svg height="50" width="120">
                            <Circle
                                cx="55"
                                cy="-6"
                                r="56"
                                fill="#B6C6E5"
                                opacity="0.9"
                            />
                            <Text style={{fontFamily:'Poppins-Regular', textAlign:'center', color:'#F1F4FA'}}>Edit photo</Text>
                        </Svg>
                    </View>
                    <Image source={{uri: image}} style={{width:110, height:110, borderRadius:100}} />
                    
                </TouchableOpacity>
            </View>

            <View style={{width:'100%', alignItems:"center", paddingHorizontal:20,}}>
                <View style={{width:'100%', backgroundColor:'#FAFCFF', elevation:5, padding:20, borderRadius:20}}>
                    <View style={{width:21, height:21, alignSelf:'flex-end'}}>
                        <TouchableOpacity onPress={() => {
                                setEditState(!editState)
                                resetData()
                            }}>
                            <Image 
                                source={
                                    editState ? require('../Assets/Icons/CrossIcon.png') 
                                    : require('../Assets/Icons/EditIcon.png')} 
                                style={{width:21, height:21, tintColor:globalStyle.secondaryColorDark}}
                            />
                        </TouchableOpacity>
                    </View>
                    <Text style={{fontFamily:"Poppins-ExtraLight", marginVertical:2}}>First Name</Text>
                    <ProfileInputField
                        value={firstname}
                        onChangeText={(text) => setFirstname(text)}
                        hideBorder={!editState} 
                        caretHidden={!editState} 
                        editable={editState}
                    />
                    <Text style={{fontFamily:"Poppins-ExtraLight", marginVertical:2}}>Last Name</Text>
                    <ProfileInputField 
                        value={lastname}
                        onChangeText={(text) => setLastname(text)}
                        hideBorder={!editState} 
                        caretHidden={!editState} 
                        editable={editState}
                    /> 
                    <Text style={{fontFamily:"Poppins-ExtraLight", marginVertical:2}}>Username</Text>
                    <ProfileInputField
                        value={username}
                        onChangeText={(text) => setUsername(text)}
                        hideBorder={!editState} 
                        caretHidden={!editState} 
                        editable={editState}
                    />
                    <Text style={{fontFamily:"Poppins-ExtraLight", marginVertical:2}}>Email</Text>
                    <ProfileInputField
                        value={email}
                        hideBorder={!editState} 
                        caretHidden={!editState} 
                        editable={false}
                    />
                </View>

                <View style={{width:'100%', marginTop:20, justifyContent:"space-between"}}>
                    { editState 
                    ? <PrimaryButtonMed 
                        title="Save"
                        containerStyle={{width:'25%', height:30, alignSelf:'flex-end'}}
                        textStyle={{fontSize:14}}
                        onPress={() => handleUpdateProfile()}
                    />  
                    : <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                        <TouchableOpacity onPress={() => setLogoutModal(true)} style={{flexDirection:'row', marginHorizontal:10}}>
                            <Icon name='logout' size={20} color={'#625BAD'} /> 
                            <Text style={{fontFamily:'Poppins-Regular', justifyContent:'center'}}>Logout</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => setModalChangePassword(true)}>
                            <Text style={{fontFamily:'Poppins-Regular'}}>Change Password</Text>
                        </TouchableOpacity>
                    </View>
                }
                    
                </View>
            </View>
            {/*MODAL SECTION*/}
            <ChangePhotoModal 
                onRequestClose={() => setModalImage(false)}
                onBackdropClick={()=> setModalImage(false)}
                modalVisible={modalImage}
                onPressTake={() => handleFromCamera()}
                onPressAdd={() => handleFromLibrary()}
                onPressRemove={() => handleRemoveImage()}
            />

            <Modal 
                animationType="fade"
                visible={alertModal}
                transparent={true}
                >
                <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

                    <TouchableWithoutFeedback>
                        <View style={styles.modalOverlay}/>
                    </TouchableWithoutFeedback> 
                    
                    <View style={styles.alertCard}>
                        {loadingProfile 
                        ? <LoadingCard />
                        : message == 'success' ? <AlertCard 
                                                    cardTitle={true}
                                                    cardTitleText={`Update Profile Successfully`}
                                                    cardImage={require('../Assets/Icons/RoundCheckIcon.png')}
                                                    cardText={`We have successfully updated your profile`}
                                                    buttonTitle="Ok"
                                                    onPressCardButton={() => {
                                                        setAlertModal(false)
                                                        dispatch({type: 'ALERT_CLOSE_OK'})
                                                        setEditState(false)
                                                    }}
                                                />
                        : <AlertCard 
                            cardTitle={true}
                            cardTitleText={`Update Profile Failed`}
                            cardImage={require('../Assets/Icons/RoundCrossIcon.png')}
                            cardText={message == "password didn't match" ? "Old password didn't match" : `Sorry, we couldn't update your profile right now`}
                            buttonTitle="Ok"
                            onPressCardButton={() => {
                                setAlertModal(false)
                                dispatch({type: 'ALERT_CLOSE_OK'})
                            }}
                        />
                        }
                        
                    </View>
            </Modal>

            <PasswordModal 
                modalVisible={modalChangePassword}
                onRequestClose={() => {
                    setModalChangePassword(false)
                    setOldPassword('')
                    setNewPassword('')
                    setConfirmPassword('')
                }}
                onBackdropClick={() => {
                    setModalChangePassword(false)
                    setOldPassword('')
                    setNewPassword('')
                    setConfirmPassword('')
                }}

                onChangeoldPassword={(text) => setOldPassword(text)}
                valueOldPassword={oldPassword}
                onSubmitEditingOld={() => {
                    newPasswordRef.current.focus()
                }}

                validator={
                    newPassword == '' ? null :
                    newPassword.match(passwordFormat) 
                    ?   <Text style={{color:'green'}}>Good password!</Text>
                    :   <Text style={{color:'red'}}>Bad Password!</Text>
                    }
                refPasswordNew={newPasswordRef}                     
                onChangeNewPassword={(text) => setNewPassword(text)}
                valueNewPassword={newPassword}
                onSubmitEditingNew={() => {
                    if(newPassword.match(passwordFormat)) {
                        passwordRef.current.focus()
                    } else {
                        alert('Password must contain one Capital letter, one number, and one special case and minimum 8 character')
                    }
                }}
                
                validatorConfirm={
                    confirmPassword == ''? null : confirmPassword.match(newPassword) ? <Text style={{color:'green'}}>Password match!</Text> 
                    : <Text style={{color:'red'}}>Password doesn't match!</Text>
                }
                refConfirm={passwordRef}
                onChangeConfirmPassword={(text) => setConfirmPassword(text)}
                valueConfirmPassword={confirmPassword}
                onSubmitEditingConfirm={() => Keyboard.dismiss()}
                
                onPressSave={() => handleChangePassword()}
            />

            <AlertModal 
                alertModalVisible ={logoutModal}
                onBackdropClick={() => setLogoutModal(false)}
                cardImage={require('../Assets/Icons/QuestionMarkIcon.png')}
                cardText={"Are you sure want to quit remindme?"}
                cardTypeAsk={true}
                cardPosition={{
                    top:'30%'
                }}
                cardStyle={{
                    height:200
                }}
                onPressButtonYes={() => handleLogout()}
                onPressButtonNo={() => setLogoutModal(false)}
            />

        </View>
        </KeyboardAvoidingView>
    )
}

export default Profile

const styles = StyleSheet.create({
    backgroundBase : {
        width:Dimensions.get('screen').width,
        height:Dimensions.get('screen').height,
        backgroundColor: '#F1F4FA',
        alignItems:'center',
        // paddingHorizontal:20
    },
    oval : {
        position: 'absolute',
        width: 500,
        height: 200,
        backgroundColor:'#625BAD',
        borderBottomLeftRadius: 500,
        borderBottomRightRadius: 500,
    },
    modalOverlay : {
        flex:1,
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
        backgroundColor:'transparent'
    },
    alertCard : {
        position:'absolute',
        left:'15%',
        right:'15%',
        top:'25%',
        height:'40%',
    }
})
