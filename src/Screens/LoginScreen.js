import React, { useEffect, useState } from 'react'
import { ActivityIndicator, Dimensions, Image, KeyboardAvoidingView, Linking, Modal, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import { PrimaryButtonLarge, PrimaryButtonMed} from '../Components/PrimaryButton'
import {InputField} from '../Components/InputField'
import PasswordField from '../Components/PasswordField'
import { SecondaryButtonLarge } from '../Components/SecondaryButton'
import { AlertModal, EmailModal } from '../Components/Modal'
import { useDispatch, useSelector } from 'react-redux'
import { BlurView } from '@react-native-community/blur'
import { AlertCard, LoadingCard, PasswordCard } from '../Components/Card'


const LoginScreen = (props) => {
    const dispatch = useDispatch()
    const [userPlaceholder, setUserPlaceholder] = useState('Email/Username')
    const [passwordPlaceholder, setPasswordPlaceholder] = useState('Password')
    const [userinput, setUserinput] = useState('')
    const [password, setPassword] = useState('')
    const [forgotModal, setForgotModal] = useState(false)
    const [email, setEmail] = useState('')

    const statusCode = useSelector(state => state.auth.statusCode)
    const isLoading = useSelector(state => state.auth.loading)
    const modalState = useSelector(state => state.auth.modalState)
    const forgotModalState = useSelector(state => state.auth.forgotModalState)
    const verifyCheck = useSelector(state => state.auth.verifyCheck)
    const emailStore = useSelector(state => state.auth.emailStore)
    const message = useSelector(state => state.auth.message)
    console.log('verifyCheck', verifyCheck)
    console.log('emailStore', emailStore)
    // console.log('email/username', userinput)
    // console.log('password', password)
    // console.log('code', statusCode)
    const handleLogin = () => {
        if (userinput && password) {
            let newLogin = {
                emailorusername : userinput,
                password : password
            }
            dispatch({type: 'LOGIN', dataLogin: newLogin})
        } else {
            alert("Username/Email/Password cannot be empty")
        }
        
    }


    const handleSendEmail = () => {
        if (email) {
            dispatch({type : 'FORGOT_SEND', email : email})
            // props.navigation.navigate('Forgot')
        } else {
            alert("email should not be empty")
        }
    }



    return (
        <KeyboardAvoidingView enabled={!forgotModal} behavior='position' style={{flex:1}}>
        <View style={styles.screenConfig}>
            <Text style={{fontFamily: "Poppins-Regular", fontSize:20}}>Sign In</Text>
            <Image 
            source={require('../Assets/Images/LoginScreen.png')} 
            style={{width:"50%", height:'40%', marginVertical:5}}
            />
            <View style={{width:'100%', height:'20%', justifyContent:"space-evenly",alignItems:"center"}}>
                <InputField 
                title={userPlaceholder}
                onFocus={() => setUserPlaceholder('')}
                onBlur={() => setUserPlaceholder('Email/Username')}
                onChangeText={(text) => setUserinput(text)}
                value={userinput}
                />
                <PasswordField 
                title={passwordPlaceholder}
                onFocus={() => setPasswordPlaceholder('')}
                onBlur={() => setPasswordPlaceholder('Password')}
                onChangeText={(text) => setPassword(text)}
                value={password}
                />
            </View>

            <TouchableWithoutFeedback onPress={() => dispatch({type: 'OPEN_FORGOT'})}>
                <Text style={{alignSelf:'flex-start', paddingHorizontal:5, fontFamily:'Poppins-Regular', paddingVertical:0}}>Forgot Password?</Text>
            </TouchableWithoutFeedback>

            <View style={{width:'100%', alignItems:'center', marginVertical:20}}>
                <PrimaryButtonLarge
                title="Submit"
                onPress={() => handleLogin()}
                />
            </View>


             <View style={{flexDirection:'row', alignItems:'center', justifyContent:"center", marginVertical:10,}}>
                <Text style={{fontFamily:'Poppins-Regular', fontSize:16}}>
                    {"Don't have an account yet ? "}
                    <TouchableWithoutFeedback onPress={() => props.navigation.navigate('Register')}>
                        <Text style={{textDecorationLine:'underline', fontFamily:'Poppins-Regular', fontSize:16}}>Sign Up!</Text>
                    </TouchableWithoutFeedback>
                </Text>
             </View>

             {/* <SecondaryButtonLarge 
                title="Try"
                onPress={() => props.navigation.navigate('Forgot')}
             /> */}

            {/* modal */}
            <Modal 
            animationType="fade"
            visible={modalState}
            transparent={true}
            >
                <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

                    <TouchableWithoutFeedback
                        onPress={() => setModalTry(false)}
                    >
                        <View style={styles.modalOverlay}/>
                    </TouchableWithoutFeedback>
                    
                    <View style={styles.alertCard}>
                        {isLoading ? <LoadingCard />
                        : statusCode == '401' && verifyCheck === false 
                        ? <AlertCard 
                                cardTitle={true}
                                cardTitleText="Login Failed"
                                cardImage={require('../Assets/Icons/RoundCrossIcon.png')}
                                cardText="You have not verified your account"
                                buttonTitle="Send Verify"
                                onPressCardButton={() => dispatch({type: 'SEND_VERIFY', email: emailStore})}
                                />
                        : statusCode == '401' 
                        ? <AlertCard 
                            cardTitle={true}
                            cardTitleText="Login Failed"
                            cardImage={require('../Assets/Icons/RoundCrossIcon.png')}
                            cardText="User not found/Wrong password"
                            buttonTitle="Ok"
                            onPressCardButton={() => dispatch({type: 'FAILED_OK'})}
                            />
                        : message && statusCode == '201' 
                        ? <AlertCard 
                            cardTitle={true}
                            cardTitleText="Verification Sent"
                            cardImage={require('../Assets/Icons/RoundCheckIcon.png')}
                            cardText="We have sent verification mail to your email"
                            buttonTitle="Ok"
                            onPressCardButton={() => dispatch({type: 'FAILED_OK'})}
                            />

                        : <AlertCard 
                            cardTitle={true}
                            cardTitleText="Login Successful"
                            cardImage={require('../Assets/Icons/RoundCheckIcon.png')}
                            cardText="You have successfully Login"
                            buttonTitle="Ok"
                            onPressCardButton={() => dispatch({type: 'SUCCESS_OK'})}
                        />
                        }
                        
                    </View>

            </Modal>

            <Modal
                animationType="fade"
                visible={forgotModalState}
                transparent={true}
                onRequestClose={() => dispatch({type: 'CLOSE_FORGOT'})}
            >
                <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

                    <TouchableWithoutFeedback
                        onPress={() => dispatch({type: 'CLOSE_FORGOT'})}
                    >
                        <View style={styles.modalOverlay}/>
                    </TouchableWithoutFeedback>

                <View style={{...styles.alertCard, left:'10%', right:'10%', height:250}}>
                          
                    {isLoading 
                    ? <ActivityIndicator size='large' color='blue' />
                    : statusCode == '201' 
                    ? <AlertCard 
                        cardTitle={true}
                        cardTitleText="Email sent"
                        cardImage={require('../Assets/Icons/RoundCheckIcon.png')}
                        cardText={message}
                        buttonTitle="Ok"
                        onPressCardButton={() => {
                            dispatch({type: 'FORGOT_OK'})
                            props.navigation.navigate('Forgot')
                        }}
                    /> 
                    : statusCode 
                    ? <AlertCard 
                        cardTitle={true}
                        cardTitleText="Verification Failed"
                        cardImage={require('../Assets/Icons/RoundCrossIcon.png')}
                        cardText={message}
                        buttonTitle="Ok"
                        onPressCardButton={() => dispatch({type: 'FORGOT_OK'})}
                    />
                     : <PasswordCard 
                        emailState={true}
                        onPressSave={() => handleSendEmail()}
                        onChangeEmail={(text) => setEmail(text)}
                        valueEmail={email}
                    />
                    }

                </View>

            </Modal>


        </View>
        </KeyboardAvoidingView>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    screenConfig: {
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
        alignItems:'center', 
        backgroundColor:'#F1F4FA',
        paddingHorizontal:20
    },
    alertCard : {
        position:'absolute',
        left:'15%',
        right:'15%',
        top:'25%',
        height:'40%',
    },
    modalOverlay : {
        flex:1,
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
        backgroundColor:'transparent'
    },
})
