import React, { useState } from 'react'
import { StyleSheet, Text, View, Dimensions, TouchableWithoutFeedback, Image, KeyboardAvoidingView, } from 'react-native'
import { useDispatch } from 'react-redux'
import { HeaderWithText } from '../Components/Header'
import {InputField} from '../Components/InputField'
import { OutlineButtonMed } from '../Components/OutlineButton'
import { PrimaryButtonMed } from '../Components/PrimaryButton'
import globalStyle from '../Util/globalStyle'

const RegisterTwo = (props) => {
    const dispatch = useDispatch()
    const [lastnamePlaceholder, setLastnamePlaceholder] = useState('Last Name')
    const [lastname, setLastname] = useState('')
    console.log(lastname)

    const handleNext = () => {
        if (lastname !== '') {
            dispatch({type:'LAST_NAME', lastName: lastname})
            console.log('lastname pushed')
            props.navigation.navigate('RegisPageThree')    
        } else {
            alert('Lastname must not empty')
        }
    }

    return (
        <KeyboardAvoidingView behavior="position" style={{flex:1}}>
            <View style={styles.screenConfig}>

                <HeaderWithText 
                    onPressBack={() => props.navigation.navigate('RegisPageOne')}
                    title='Sign Up'
                />

                <View style={{width:'100%'}}>
                    <Text style={styles.sectionText}>Last Name</Text>
                    <Text style={styles.sectionSubText}>Let we know you better, with entering your last name.</Text>
                </View>

                <Image source={require('../Assets/Images/Register-2.png')} style={{width:'100%', height:'40%', marginVertical:10}}/>
                
                <View style={{width:'100%', alignItems:'center'}}>
                    <InputField 
                        title={lastnamePlaceholder}
                        onFocus={() => setLastnamePlaceholder('')}
                        onBlur={() => setLastnamePlaceholder('Last Name')}
                        onChangeText={(text) => setLastname(text)}
                        value={lastname}
                        />
                </View>

                <View style={{
                    width:'100%', 
                    marginVertical:40, 
                    alignItems:'center', 
                    flexDirection:"row", 
                    justifyContent:'space-evenly'
                    }}>
                    <OutlineButtonMed 
                    title="Previous"
                    containerStyle={{marginHorizontal:20}}
                    onPress={() => props.navigation.navigate('RegisPageOne')}
                    />
                    <PrimaryButtonMed 
                        title="Next"
                        containerStyle={{marginHorizontal:20}}
                        onPress={() => handleNext()}
                    />
                </View>

            </View>
        </KeyboardAvoidingView>
    )
}

export default RegisterTwo

const styles = StyleSheet.create({
    screenConfig: {
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
        alignItems:'center', 
        backgroundColor:globalStyle.primaryColor,
        paddingHorizontal:20
    },
    sectionText : {
        fontFamily: "Poppins-medium",
        fontSize:24,
        color:globalStyle.TCdark
    },
    sectionSubText : {
        fontFamily: "Poppins-ExtraLight", 
        fontSize:16, 
        color:globalStyle.TCdark
    }
})
