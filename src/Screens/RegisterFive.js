import { BlurView } from '@react-native-community/blur'
import React, { useState } from 'react'
import { StyleSheet, Text, View, Dimensions, TouchableWithoutFeedback, Image, KeyboardAvoidingView, Modal, } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { AlertCard, LoadingCard } from '../Components/Card'
import { HeaderWithText } from '../Components/Header'
import {InputField} from '../Components/InputField'
import { AlertModal } from '../Components/Modal'
import { OutlineButtonMed } from '../Components/OutlineButton'
import PasswordField from '../Components/PasswordField'
import { PrimaryButtonMed } from '../Components/PrimaryButton'
import globalStyle from '../Util/globalStyle'

const passwordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/

const RegisterThree = (props) => {
    const dispatch = useDispatch()
    const [passwordPlaceholder, setPasswordPlaceholder] = useState('example : mYpas5word!')
    const [password, setPassword] = useState('')

    const modalState = useSelector(state => state.register.modalState)
    const statusCode = useSelector(state => state.register.statusCode)
    const message = useSelector(state => state.register.message)
    const isLoading = useSelector(state => state.register.isLoading)
    const firstName = useSelector(state => state.register.firstName)
    const lastName = useSelector(state => state.register.lastName)
    const userName = useSelector(state => state.register.userName)
    const userEmail = useSelector(state => state.register.email)



    const handleVerify = () => {
        if(password.match(passwordFormat)) {
            let newData = {
                firstname : firstName,
                lastname : lastName,
                username : userName,
                email : userEmail,
                password : password,
            }
            dispatch({type: 'REGISTER', signupData : newData})
        } else {
            alert('Password must contain one Capital letter, one number, and one special case and minimum 8 character')
        }
        
    }

    return (
        <KeyboardAvoidingView behavior="position" style={{flex:1}}>
        <View style={styles.screenConfig}>

            <HeaderWithText 
                onPressBack={() => props.navigation.navigate('RegisPageFour')}
                title='Sign Up'
            />

            <View style={{width:'100%'}}>
                <Text style={styles.sectionText}>Password</Text>
                <Text style={styles.sectionSubText}>Don't let anyone know, when you enter your password.</Text>
            </View>
            
            <Image source={require('../Assets/Images/Register-5.png')} style={{width:'100%', height:'40%', marginVertical:10}}/>
            
            

            <View style={{width:'100%', alignItems:'center', marginVertical:10}}>
                <PasswordField 
                    title={passwordPlaceholder}
                    onFocus={() => setPasswordPlaceholder('')}
                    onBlur={() => setPasswordPlaceholder('example : mYpas5word!')}
                    onChangeText={(text) => setPassword(text)}
                    value={password}
                    />
            </View>

            {password === '' ? null : password.match(passwordFormat) 
            ? <Text style={{color:'green'}}>Good password!</Text> 
            : <Text style={{color:'red'}}>Bad Password!</Text>
            }

            <View style={{width:'100%', marginVertical:10}}>
                <Text style={{fontFamily:"Poppins-Light", fontSize:12, textAlign:'center'}}>We will send you a verification link to your registered email</Text>
            </View>

            <View style={{width:'100%', marginVertical:10, alignItems:'center', flexDirection:"row", justifyContent:'space-evenly'}}>
                <OutlineButtonMed 
                    title="Previous"
                    containerStyle={{marginHorizontal:20}}
                    onPress={() => props.navigation.navigate('RegisPageFour')}
                />
                <PrimaryButtonMed 
                    title="Verify"
                    containerStyle={{marginHorizontal:20}}
                    onPress={() => handleVerify()}
                />
            </View>

            <Modal 
                animationType="fade"
                visible={modalState}
                transparent={true}
            >
                <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

                    <TouchableWithoutFeedback
                        onPress={
                            props.onBackdropClick
                        }
                    >
                        <View style={styles.modalOverlay}/>
                    </TouchableWithoutFeedback>
                    
                    <View style={{...styles.alertCard,}}>
                        {isLoading 
                        ? <LoadingCard />
                        : statusCode == '200' || statusCode == '201' 
                        ?   <AlertCard 
                                cardTitle={true}
                                cardTitleText={'Last Step: Verify'}
                                cardImage={require('../Assets/Icons/RoundCheckIcon.png')}
                                cardText={'We have successfully register your account, Please Check your email'}
                                cardStyle={props.cardStyle}
                                buttonTitle={'Ok'}
                                onPressCardButton={() => {
                                    dispatch({type: 'REGISTER_SUCCESS_OK'})
                                    props.navigation.navigate('Login')
                                }}
                            />
                        : <AlertCard 
                            cardTitle={true}
                            cardTitleText={'Register Failed'}
                            cardImage={require('../Assets/Icons/RoundCrossIcon.png')}
                            cardText={message}
                            buttonTitle={'Ok'}
                            onPressCardButton={() => {
                                dispatch({type: 'REGISTER_FAILED_OK'})
                            }}
                            />
                        }
                        
                    </View>

            </Modal>
        </View>
        </KeyboardAvoidingView>
    )
}

export default RegisterThree

const styles = StyleSheet.create({
    screenConfig: {
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
        alignItems:'center', 
        backgroundColor:globalStyle.primaryColor,
        paddingHorizontal:20
    },
    sectionText : {
        fontFamily: "Poppins-medium",
        fontSize:24,
        color:globalStyle.TCdark
    },
    sectionSubText : {
        fontFamily: "Poppins-light", 
        fontSize:16, 
        color:globalStyle.TCdark
    },
    alertCard : {
        position:'absolute',
        left:'15%',
        right:'15%',
        top:'25%',
        height:'40%',
    },
})
