import React, { useRef, useState } from 'react'
import { Dimensions, KeyboardAvoidingView, Modal, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker'
import { HeaderWithText } from '../Components/Header'
import {InputField, SmallInputField} from '../Components/InputField'
import {  PrimaryButtonLarge, PrimaryButtonMed } from '../Components/PrimaryButton'
import {  SecondaryButtonMed } from '../Components/SecondaryButton'
import moment from 'moment'
import MonthlyCalendarComponent from '../Components/MonthlyCalendarComponent'
import ColorPicker from '../Components/ColorPicker'
import { useDispatch, useSelector } from 'react-redux'
import { BlurView } from '@react-native-community/blur'
import { AlertCard, LoadingCard } from '../Components/Card'
import { getRandomColor } from '../Util/RandomColor'
import globalStyle from '../Util/globalStyle'

const GoalsScreen = (props) => {
    const dispatch = useDispatch()
    const [keyboardBehavior, setKeyboardBehavior] = useState(true)
    const loadingPost = useSelector(state => state.goals.isLoadingPost)
    const loadingEdit = useSelector(state => state.goals.isLoadingEdit)
    const statusCode = useSelector(state => state.goals.statusCode)
    const modalState = useSelector(state => state.goals.modalState)
    const modalStateEdit = useSelector(state => state.goals.modalStateEdit)

    // calendar section hooks
    const today = moment().format('Y-MM-DD')
    const [selectedDay, setSelectedDay] = useState(typeof props.route.params == 'undefined' ? today : moment.utc(props.route.params.goalsData.date).format("Y-MM-DD"))
    // const [calendarValue, setCalendarValue] = useState(today)
    const [openCalendar, setOpenCalendar] = useState(false)
    const dateTime = moment.utc(selectedDay, "Y-MM-DD").format()

    // goals input hooks
    const [goalsInputPlaceholder, setGoalsInputPlaceholder] = useState(typeof props.route.params == 'undefined' ? 'What is Your Goal?' : props.route.params.goalsData.name)
    const [goalsInput, setGoalsInput] = useState(typeof props.route.params == 'undefined' ? '' : props.route.params.goalsData.name)
    const [goalsType, setGoalsType] = useState(typeof props.route.params == 'undefined' ? 'build' : props.route.params.goalsData.goal_type)
    const [calendarItems, setCalendarItems] = useState([
        {label: 'Apple', value: 'apple'},
    ]);

    // dropdown hooks
    const [open, setOpen] = useState(false);
    const [valueType, setValueType] = useState(typeof props.route.params == 'undefined' ? null : props.route.params.goalsData.target_type);
    const [goalsValue, setGoalsValue] = useState(typeof props.route.params == 'undefined' ? '' : props.route.params.goalsData.target.toString())
    const [items, setItems] = useState([
        {label: 'Hours', value: 'Hours'},
        {label: 'Minutes', value: 'Minutes'},
        {label: 'Times', value: 'Times'},
        {label: 'Liter', value: 'Liter'},
        {label: 'Mililiter', value: 'Mililiter'},
        {label: 'Kilometer', value: 'Kilometer'},
        {label: 'Meter', value: 'Meter'},
        {label: 'Plate', value: 'Plate'},
        {label: 'Drink', value: 'Drink'},
        {label: 'Other', value: 'Other'},
    ]);
    console.log('item', items)
    // color picker
    const [colorPickerState, setColorPickerState] = useState(typeof props.route.params == 'undefined' ? '' : props.route.params.goalsData.color)
    // console.log(props.route.params)
    // console.log('=========================================================')
    // console.log('goals input', goalsInput)
    // console.log('goalsType', goalsType)
    // console.log('date', dateTime)
    // console.log('valueType', valueType)
    // console.log('goals value', goalsValue)
    // console.log('color bar', colorPickerState)
    // console.log(statusCode)

    // handleData
    const handleSaveData = () => {
        if (goalsInput === '') {
            alert("My Goals and Set Value cannot be empty")
        } else if (parseInt(goalsValue) < 1){
            alert("Value must not 0")
        } else {     
            let newGoals = {
                name : goalsInput,
                goal_type: goalsType,
                date: dateTime,
                target: goalsValue,
                target_type: valueType,
                color: colorPickerState === '' ? getRandomColor() : colorPickerState
            }
            dispatch({type: 'POST_GOALS', dataGoals: newGoals})
        }
        
    }

    const handleEditData = () => {
        if (goalsInput === '') {
            alert("My Goals and Set Value cannot be empty")
        } else if (parseInt(goalsValue) < 1){
            alert("Value must not 0")
        } else {     
            let newGoals = {
                name : goalsInput,
                date : dateTime,
                target: goalsValue,
                color: colorPickerState
            }
            dispatch({type: "EDIT_GOALS", editPayload : {
                                            editGoalsData: newGoals, 
                                            goalsId: typeof props.route.params == 'undefined' ? null : props.route.params.goalsData.id
                                        }})
        }
    }

    // console.log('fuck', value)

    // const colorState = useSelector(state => state.colors.colorState)

    return (
        <KeyboardAvoidingView enabled={keyboardBehavior} behavior='position' style={{flex:1}}>
        <View style={{...styles.backgroundBase, justifyContent:'space-between'}}>

                {/* Main Content */}
                <View style={{width:'100%',height:'90%', justifyContent:'space-between',}}>
                    
                    {/* Top Content */}
                    <View style={{width:'100%',}}>
                    <HeaderWithText 
                        onPressBack={() => props.navigation.navigate('Home')}
                    />
                    
                    
                    <View style={{width:'100%', alignItems:"center", marginBottom:10}}>
                        <Text style={{ 
                            alignSelf:'flex-start', 
                            fontFamily:"Poppins-Regular",
                            fontSize:18
                            }}>My Goals</Text>
                        <InputField 
                        
                            title={goalsInputPlaceholder}
                            onFocus={() => {
                                setGoalsInputPlaceholder(''),
                                setKeyboardBehavior(false)
                            }}
                            onBlur={() => {
                                setGoalsInputPlaceholder('What is Your Goal?')
                            }}
                            onChangeText={(text) => setGoalsInput(text)}
                            value={goalsInput}
                            // onSubmitEditing={() => setKeyboardBehavior(true)}
                        />
                    </View>
                    

                    <View style={{width:'100%', alignItems:'center', marginVertical:10}}>
                        <Text style={{
                            alignSelf:'flex-start',
                            fontFamily:"Poppins-Regular",
                            fontSize:18,
                        }}>Build or Quit This Goal?</Text>
                        <View style={{width:'100%', flexDirection:"row", justifyContent:'space-evenly', paddingHorizontal:5}}>
                            <PrimaryButtonMed 
                                title="Build"
                                containerStyle={{height:40, marginHorizontal:20, 
                                    backgroundColor: goalsType == 'build' ? globalStyle.secondaryColorDark : globalStyle.secondaryColorLight}}
                                textStyle={{fontSize:14}}
                                onPress={() => setGoalsType('build')}
                            />
                            <SecondaryButtonMed 
                                title="Quit"
                                containerStyle={{height:40, marginHorizontal:20,
                                    backgroundColor: goalsType == 'quit' ? globalStyle.secondaryColorDark : globalStyle.secondaryColorLight}}
                                textStyle={{fontSize:14}}
                                onPress={() => setGoalsType('quit')}
                            />
                        </View>
                    </View>

                    <View style={{width:'100%', alignItems:"center", marginVertical:10}}>
                        <Text style={{ 
                            alignSelf:'flex-start',
                            fontFamily:"Poppins-Regular",
                            fontSize:18
                            }}>Select Date
                        </Text>

                        {/* calender picker */}
                        <View style={{width:"100%",}}>

                            <DropDownPicker 
                                    items={calendarItems}
                                    value={selectedDay}
                                    open={openCalendar}
                                    
                                    placeholder={selectedDay}
                                    setOpen={() => setOpenCalendar(!openCalendar)}
                                    // setValue={(item)=> setCalendarValue(item)}
                                    dropDownContainerStyle={{
                                        height:0,
                                        borderWidth:0
                                    }}
                                    style={{
                                        backgroundColor:'#F1F4FA',
                                        width:'50%',
                                        height:30,
                                        borderColor:'#B6C6E5'
                                    }}
                                    textStyle={{
                                        fontFamily:'Poppins-Regular'
                                    }}
                            />
                            { openCalendar
                            ? 
                            <View style={{width:'100%'}}>
                                <MonthlyCalendarComponent
                                    onDayPress={(day) => {
                                        setSelectedDay(day.dateString),
                                        setOpenCalendar(false)
                                    }}
                                    hideArrows={true}
                                    enableSwipeMonths={true}
                                    disableArrowLeft={true}
                                    disableArrowRight={true}
                                    hideExtraDays={true}
                                    markedDates={{
                                    [selectedDay]: {
                                        selected: true,
                                        selectedColor: '#625BAD',
                                        selectedTextColor: '#F1F4FA'
                                        }
                                    }} 
                                    theme={{
                                        backgroundColor:"#F1F4FA",
                                        calendarBackground:'#F1F4FA',
                                        textSectionTitleColor:'#342D50',
                                        textDayFontFamily: 'Poppins-Regular',
                                        textDayHeaderFontFamily: 'Poppins-Regular',
                                        disabledArrowColor:'#B6C6E5',
                                        textDayFontSize:12,
                                        textDayHeaderFontSize:12,
                                        textDisabledColor: 'rgba(52, 45, 80, 0.5)',
                                        'stylesheet.calendar.header' : {
                                            monthText : {
                                                fontSize: 13,
                                                width:'100%',
                                                alignSelf: 'flex-start',
                                                marginVertical:10,
                                                fontFamily:'Poppins-Regular',
                                                color:'#342D50'
                                            },
                                        },
                                        'stylesheet.calendar.main' : {
                                            week: {
                                                flexDirection: 'row',
                                                justifyContent: 'space-around'
                                            },
                                        }
                                    }}/>
                                </View> : null
                            }
                        </View>      
                    </View>
                    
                    
                    {/* Value picker */}
                    <View style={{width:'100%', marginTop:5}}>
                        <Text style={{
                            fontFamily:"Poppins-Regular",
                            fontSize:18
                        }}>Set Value
                        </Text>
                        <DropDownPicker 
                            items={items}
                            value={valueType}
                            open={open}
                            placeholder=""
                            disabled={typeof props.route.params == 'undefined' ? false : true}
                            setOpen={() => setOpen(!open)}
                            setItem={() => console.log('waw')}
                            setValue={(item)=> setValueType(item)}
                            listItemLabelStyle={{
                                fontFamily:'Poppins-Regular',
                            }}
                            style={{
                                width:'50%',
                                backgroundColor:'#F1F4FA',
                                height:30,
                                borderColor:'#B6C6E5'
                            }}
                            dropDownContainerStyle={{
                                width:'50%',
                            }}
                            textStyle={{
                                fontFamily:'Poppins-Regular'
                            }}
                        />

                        {valueType !== null 
                        ? <View style={{marginVertical:10}}>
                            <Text style={{fontFamily:"Poppins-Regular",}}>Count {valueType}</Text>
                            <InputField
                                placeholder={goalsValue}
                                onFocus={() => setKeyboardBehavior(true)}
                                onChangeText={(text) => setGoalsValue(text)}
                                value={goalsValue}
                                keyboardType='numeric'
                                style={{
                                    width:'30%',
                                    alignItems:'center'
                                }}
                            />
                        </View> : null}
                    </View>

                    
                    

                    
                    {/* Color Picker */}
                    <View style={{width:'100%', marginVertical:0}} >
                        <Text style={{
                            fontFamily:"Poppins-Regular",
                            fontSize:18,
                            marginVertical: 10
                        }}>Prograss Bar Color</Text>
                        <View style={{flexDirection:'row', width:'100%', justifyContent:'space-between'}}>
                            <ColorPicker 
                                colors = "#FFBCC2"
                                pickerState={colorPickerState === "#FFBCC2" ? true : false}
                                onPress={() => setColorPickerState('#FFBCC2')}
                            />
                            <ColorPicker 
                                colors = "#FCF3A1"
                                pickerState={colorPickerState === "#FCF3A1" ? true : false}
                                onPress={() => setColorPickerState('#FCF3A1')}
                            />
                            <ColorPicker 
                                colors = "#CCF0D7"
                                pickerState={colorPickerState === "#CCF0D7" ? true : false}
                                onPress={() => setColorPickerState('#CCF0D7')}
                            />
                            <ColorPicker 
                                colors = "#FF8888"
                                pickerState={colorPickerState === "#FF8888" ? true : false}
                                onPress={() => setColorPickerState('#FF8888')}
                            />
                            <ColorPicker 
                                colors = "#D1CDFA"
                                pickerState={colorPickerState === "#D1CDFA" ? true : false}
                                onPress={() => setColorPickerState('#D1CDFA')}
                            />
                        </View>
                    </View>

                    
                    
                </View>

                {/* Footer */}
                {typeof props.route.params == 'undefined' 
                    ? <View style={{
                        width:'100%', 
                        paddingHorizontal:5, 
                        marginVertical:20, 
                        
                        }}>
                        <PrimaryButtonLarge 
                            title='Save'
                            onPress={() => handleSaveData()}
                        />
                    </View> 
                    : <View style={{width:'100%', alignItems:'center', paddingHorizontal:5, marginVertical:20}}>
                        <PrimaryButtonLarge 
                            title='Edit'
                            onPress={() => handleEditData()}
                        />
                    </View>
                    }


                {/* on post goal modal*/}
                <Modal 
                    animationType="fade"
                    visible={modalState}
                    transparent={true}
                    >
                    <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

                        <TouchableWithoutFeedback
                            onPress={
                                props.onBackdropClick
                            }
                        >
                            <View style={styles.modalOverlay}/>
                        </TouchableWithoutFeedback>
                        <View style={styles.alertCard}>
                            
                            {loadingPost ? <LoadingCard /> 
                            : statusCode == '201' ? <AlertCard 
                                                        cardTitle={true}
                                                        cardTitleText="Create Goals Success"
                                                        cardImage={require('../Assets/Icons/RoundCheckIcon.png')}
                                                        cardText="We Successfully created your goal"
                                                        buttonTitle="Yeay!"
                                                        onPressCardButton={() => {
                                                            dispatch({type: 'GET_DATA_GOALS'})
                                                            dispatch({type:'POST_SUCCESS_OK'}) 
                                                            props.navigation.navigate('Home')
                                                        }}
                                                    />
                            : <AlertCard 
                                cardTitle={true}
                                cardTitleText="Create Goals Failed"
                                cardImage={require('../Assets/Icons/RoundCrossIcon.png')}
                                cardText="Sorry we couldn't create your goal at the moments"
                                buttonTitle="Awww"
                                onPressCardButton={() => dispatch({type:'POST_FAILED_OK'})}
                            />
                            }
                        </View>

                </Modal>

                {/* on edit goal modal*/}
                <Modal 
                    animationType="fade"
                    visible={modalStateEdit}
                    transparent={true}
                    >
                    <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

                        <TouchableWithoutFeedback
                            onPress={
                                props.onBackdropClick
                            }
                        >
                            <View style={styles.modalOverlay}/>
                        </TouchableWithoutFeedback>
                        <View style={styles.alertCard}>
                            
                            {loadingEdit ? <LoadingCard /> 
                            : statusCode == '201' ? <AlertCard 
                                                        cardTitle={true}
                                                        cardTitleText="Edit Goals Success"
                                                        cardImage={require('../Assets/Icons/RoundCheckIcon.png')}
                                                        cardText="We Successfully Edit your goal"
                                                        buttonTitle="Yeay!"
                                                        onPressCardButton={() => {
                                                            dispatch({type: 'GET_DATA_GOALS'})
                                                            dispatch({type:'EDIT_SUCCESS_OK'}) 
                                                            props.navigation.navigate('Home')
                                                        }}
                                                    />
                            : <AlertCard 
                                cardTitle={true}
                                cardTitleText="Edit Goals Failed"
                                cardImage={require('../Assets/Icons/RoundCrossIcon.png')}
                                cardText="Sorry we couldn't Edit your goal at the moments"
                                buttonTitle="Awww"
                                onPressCardButton={() => dispatch({type:'EDIT_FAILED_OK'})}
                            />
                            }
                        </View>

                </Modal>
                          

            </View>

        </View>
        </KeyboardAvoidingView>
    )
}

export default GoalsScreen

const styles = StyleSheet.create({
    backgroundBase : {
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
        backgroundColor: '#F1F4FA',
        alignItems:'center',
        paddingHorizontal:20,
    },
    alertCard : {
        position:'absolute',
        left:'15%',
        right:'15%',
        top:'25%',
        height:'40%',
    }
})
