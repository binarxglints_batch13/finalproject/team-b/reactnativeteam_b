import React, { useRef, useState } from 'react'
import { Dimensions, Image, Keyboard, KeyboardAvoidingView, Modal, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import ColorPicker from '../Components/ColorPicker'
import { HeaderWithIcons } from '../Components/Header'
import { CalendarModal } from '../Components/Modal'
import { NotesInputBody, NotesInputTitle } from '../Components/NotesTextField'
import { PrimaryButtonLarge, PrimaryButtonMed } from '../Components/PrimaryButton'
import moment from 'moment'
import { getRandomColor } from '../Util/RandomColor'
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import { launchImageLib } from '../Util/ImagePickerFunc'
import { AlertCard, LoadingCard } from '../Components/Card'
import { BlurView } from '@react-native-community/blur'
import globalStyle from '../Util/globalStyle'
import { SecondaryButtonLarge } from '../Components/SecondaryButton'


const NotesScreen = (props) => {
    const dispatch = useDispatch()
    const readParams = props.route.params
    // editState
    const editState = useSelector(state => state.notes.notesEditState)
    console.log(editState)
    const modalCalendarState = useSelector(state => state.modal.modalCalendarState)
    const modalStateDelete = useSelector(state => state.notes.modalStateDelete)
    const modalStatePost = useSelector(state => state.notes.modalStatePost)
    const loadingPost = useSelector(state => state.notes.isLoadingPost)
    const loadingDelete = useSelector(state => state.notes.isLoadingDelete)
    const statusPost = useSelector(state => state.notes.statusCode)
    const deleteAskState = useSelector(state => state.notes.deleteAskState)
    // console.log('==========================================')
    // console.log('notesData', notesData)
    console.log("props.route", readParams)
    // notes hooks
    const [titlePlaceholder, setTitlePlaceholder] = useState(typeof readParams == 'undefined' ? 'Write your title here': readParams.notesDetailData.title)
    const [bodyPlaceholder, setBodyPlaceholder] = useState(typeof readParams == 'undefined' ? 'Tell me your story': readParams.notesDetailData.body)
    const [title, setTitle] = useState(typeof readParams == 'undefined' ? '': readParams.notesDetailData.title)
    const [bodyInput, setBodyInput] = useState(typeof readParams == 'undefined' ? '': readParams.notesDetailData.body)
    const [timeDisplayState, setTimeDisplayState] = useState(typeof readParams == 'undefined' ? null: moment.utc(readParams.notesDetailData.dateNote).format('Y MMM DD - HH:mm'))
    const [pinState, setPinState] = useState(typeof readParams == 'undefined' ? false: readParams.notesDetailData.pinned)
    const [colorState, setColorState] = useState(typeof readParams == 'undefined' ? globalStyle.primaryColor: readParams.notesDetailData.color)
    

    //calendar modal hooks
    const [selectedDay, setSelectedDay] = useState(moment().format('Y-MM-DD'))
    const [timeHour, setTimeHour] = useState()
    const [timeMin, setTimeMin] = useState()
    const minutesRef = useRef()

    const timeFormat = moment(`${timeHour} ${timeMin}`, "hh:mm").format('HH:mm');
    const dateFormat = moment(selectedDay, "Y-MM-DD").format("D MMM Y")
    const utcTime = typeof readParams == 'undefined' ? moment.utc(`${selectedDay} ${timeFormat}`,'Y-MM-DD HH:mm').format() : readParams.notesDetailData.dateNote
    // console.log('==========================================')
    // console.log(dateFormat)
    // console.log("selectedday",selectedDay)
    // console.log("timeValue", timeFormat)
    // console.log("timeDisplayState", timeDisplayState)
    // console.log(pinState)
    // console.log('colorState', colorState)
    console.log("utcTime",utcTime)
    

    // notes body behavior
    const [doneButton, setDoneButton] = useState(false)
    // edit button state
    const [editButtonHandler, setEditButtonHandler] = useState(false)

    const [imageStore, setImageStore] = useState(null)
    const [image, setImage] = useState(typeof readParams == 'undefined' ? null : readParams.notesDetailData.image)

    // console.log('image ==> ', image)
    const handleImage = (e) => {
        setImageStore(e)
        setImage(e.uri)
    }
    // console.log("ismage", imageStore)
    const handleUpload = async() => {
        let e = await launchImageLib(handleImage)
        console.log('photo', e)
    }

    const handleSaveCalendar = () => {
        if(timeHour && timeMin) {
            let newFormat = `${dateFormat} - ${timeFormat}`;
            setTimeDisplayState(newFormat)
            dispatch({type: 'CLOSE_MODAL_CALENDAR'})
        } else {
            alert('time input must not empty')
        }
        
    }

    const handlePostNotes = () => {
        if(title && bodyInput && utcTime) {
            let dataForm = new FormData()
            dataForm.append('title', title)
            dataForm.append('body', bodyInput)
            dataForm.append('dateNote', utcTime)
            dataForm.append('color', colorState === '#F1F4FA' ? getRandomColor() : colorState)
            dataForm.append('pinned', pinState)
            if (imageStore !== null) {
                dataForm.append('image', { 
                    name: imageStore.fileName,
                    type: imageStore.type,
                    uri: imageStore.uri
                })
            } 
            dispatch({type: 'POST_NOTES', newDataNotes : dataForm})
        } else {
            alert('Title/Body/Date cannot be empty')
        }
        
    }
    const handleModalPostOK = () => {
        dispatch({type: 'POST_SUCCESS_OK'})
        dispatch({type: 'GET_NOTES_DATA'})
        props.navigation.navigate('AllNotesScreen')
    }

    const handleModalEditOK = () => {
        dispatch({type: 'EDIT_SUCCESS_OK'})
        dispatch({type: 'GET_NOTES_DATA'})
        props.navigation.navigate('AllNotesScreen')
    }

    const handleDelete = () => {
        dispatch({type:"DELETE_NOTES", notesId: readParams.notesDetailData.id})
    }

    const handleEditNote = () => {
        let dataForm = new FormData()
            dataForm.append('title', title)
            dataForm.append('body', bodyInput)
            dataForm.append('dateNote', utcTime)
            dataForm.append('color', colorState === '#F1F4FA' ? getRandomColor() : colorState)
            dataForm.append('pinned', pinState)
            if (imageStore !== null) {
                dataForm.append('image', { 
                    name: imageStore.fileName,
                    type: imageStore.type,
                    uri: imageStore.uri
                })
            } 
        dispatch({type:'EDIT_NOTE', editPayload: {notesData:dataForm, notesId:readParams.notesDetailData.id}})
    }
    
    

    






    return (
        <View style={{...styles.backgroundBase, backgroundColor: colorState, }}>
            { doneButton 
            ? <PrimaryButtonMed 
                title="done"
                containerStyle={{width:100, height:30, alignSelf:'flex-end', marginVertical:20}}
                textStyle={{fontSize:14}}
                onPress={() => {
                    Keyboard.dismiss()
                    setDoneButton(false)
                }}
            />
            :<HeaderWithIcons 
                pinIcon={editState ? true : false}
                onPressPin={() => setPinState(!pinState)}
                pinStyle={pinState === true ? '#625BAD' : '#B6C6E5'}
                calendarIcon={editState ? true : false}
                imageIcon={editState ? true : false}
                onPressImage={() => {
                    console.log('pressed')
                    handleUpload()
                }}
                deleteIcon={editState ? false : true}
                onPressDelete={() => dispatch({type:'DELETE_MODAL_OPEN'})}
                onPressBack={() => editState ? props.navigation.navigate('AllNotesScreen') : props.navigation.navigate('Home')}
            />
            
            }
            

            <View style={styles.bodyContainer}>
                <View style={{width:'100%', paddingVertical:10, borderBottomWidth:1, borderBottomColor:'342D50'}}>
                    <NotesInputTitle 
                        title={titlePlaceholder}
                        onFocus={() => setTitlePlaceholder('')}
                        onBlur={() => setTitlePlaceholder('Write your title here')}
                        onChangeText={(text) => setTitle(text)}
                        value={title}
                        editable={editState ? true : false}
                        />
                        { timeDisplayState === null ? null 
                        : <Text style={{
                            fontFamily:'Poppins-Regular', 
                            fontSize:12,
                            paddingHorizontal:5
                            }}
                        >
                            {timeDisplayState}
                        </Text>
                        }
                    
                </View>
                
                <View style={{width:'100%', height:200, marginBottom:10}}>
                    <NotesInputBody 
                        title={bodyPlaceholder}
                        onFocus={() => {
                            setBodyPlaceholder('')
                            setDoneButton(true)
                        }}
                        onBlur={() => {
                            setBodyPlaceholder('Tell me your story..')
                            setDoneButton(false)
                        }}
                        onChangeText={(text) => setBodyInput(text)}
                        value={bodyInput}
                        editable={editState ? true : false}
                        style={{height:180}}
                        // blurOnSubmit={true}
                        // onSubmitEditing={() => {Keyboard.dismiss()}}
                        // onKeyPress={({nativeEvent}) => console.log('sesuatu',nativeEvent.key)}
                    />
                    
                </View>
                {image !== null 
                ? <View style={{width:100, height:100}}>
                    <Image source={{uri: image}} style={{width:100,height:100}}/>
                 </View> : null}
            </View>
            {
                editState 
                ?   (<View style={{width:'100%'}}>
                        <View style={styles.colorPickerContainer}>
                            <ColorPicker 
                                colors = "#FFBCC2"
                                pickerState={colorState === "#FFBCC2" ? true : false}
                                onPress={() => setColorState('#FFBCC2')}
                            />
                            <ColorPicker 
                                colors = "#FCF3A1"
                                pickerState={colorState === "#FCF3A1" ? true : false}
                                onPress={() => setColorState('#FCF3A1')}
                            />
                            <ColorPicker 
                                colors = "#CCF0D7"
                                pickerState={colorState === "#CCF0D7" ? true : false}
                                onPress={() => setColorState('#CCF0D7')}
                            />
                            <ColorPicker 
                                colors = "#FF8888"
                                pickerState={colorState === "#FF8888" ? true : false}
                                onPress={() => setColorState('#FF8888')}
                            />
                            <ColorPicker 
                                colors = "#D1CDFA"
                                pickerState={colorState === "#D1CDFA" ? true : false}
                                onPress={() => setColorState('#D1CDFA')}
                            />
                        </View>
                        {typeof readParams == 'undefined' 
                        ?   (<View style={{width:'100%', marginVertical:20, alignItems:'center'}}>
                                <PrimaryButtonLarge 
                                title="Save"
                                containerStyle={{marginHorizontal:10}}
                                onPress={() => handlePostNotes()}
                                />
                            </View> )
                        :   (<View style={{width:'100%', marginVertical:20, alignItems:'center'}}>
                                <PrimaryButtonLarge 
                                title="Edit"
                                containerStyle={{marginHorizontal:10}}
                                onPress={() => handleEditNote()}
                                />
                            </View>)
                            }
                    </View>)
                :   (<View style={{width:'100%', height:'20%', justifyContent:'flex-end', paddingVertical:10}}>
                        <PrimaryButtonLarge 
                            title="Edit"
                            onPress={() => {
                                dispatch({type: "EDIT_STATE"})
                                setEditButtonHandler(true)
                            }}
                        />
                    </View>)
            }
            

            <CalendarModal 
                // calendar props
                modalVisible={modalCalendarState}
                onDayPress={(day) => setSelectedDay(day.dateString)}
                hideArrows={true}
                enableSwipeMonths={false}
                disableArrowLeft={true}
                disableArrowRight={true}
                markedDates={{
                [selectedDay]: {
                    selected: true,
                    selectedColor: '#625BAD',
                    selectedTextColor: '#F1F4FA'
                    }
                }}

                //setTime props
                onChangeHour={(text) => setTimeHour(text)}
                onChangeMin={(text) => setTimeMin(text)}
                onSubmitEditingHour={() => {
                    if (timeHour >= 24) {
                        setTimeHour('23')
                        minutesRef.current.focus()
                    } else {
                        minutesRef.current.focus()
                    }
                }}
                onSubmitEditingMin={() => {
                    if (timeMin >= 60) {
                        setTimeMin('59')
                    }
                }}
                refMin = {minutesRef}
                maxLengthHour={2}
                maxLengthMin={2}
                valueHour={timeHour}
                valueMin={timeMin}

                //button props
                onPress={() =>{
                    handleSaveCalendar()
                    
                }}
            />
            {/* post modal */}
            <Modal 
                animationType="fade"
                visible={modalStatePost}
                transparent={true}
                >
                <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

                    <TouchableWithoutFeedback>
                        <View style={styles.modalOverlay}/>
                    </TouchableWithoutFeedback> 
                    
                    <View style={styles.alertCard}>
                        {loadingPost 
                        ? <LoadingCard />
                        : statusPost == 'success' ? <AlertCard 
                                                    cardTitle={true}
                                                    cardTitleText={`${editButtonHandler ? "Edit" : "Post" } Notes Successfully`}
                                                    cardImage={require('../Assets/Icons/RoundCheckIcon.png')}
                                                    cardText={`We have successfully ${editButtonHandler ? "edit" : "post" } your note`}
                                                    buttonTitle="Ok"
                                                    onPressCardButton={() => {
                                                        editButtonHandler ? handleModalEditOK() : handleModalPostOK()
                                                    }}
                                                />
                        : <AlertCard 
                            cardTitle={true}
                            cardTitleText={`${editButtonHandler ? "Edit" : "Post" } Notes Failed`}
                            cardImage={require('../Assets/Icons/RoundCrossIcon.png')}
                            cardText={`Sorry, we couldn't ${editButtonHandler ? "edit" : "post" } your note right now`}
                            buttonTitle="Ok"
                            onPressCardButton={() => {
                                editButtonHandler ? dispatch({type: 'EDIT_FAILED_OK'}) : dispatch({type:'POST_FAILED_OK'})
                            }}
                        />
                        }
                        
                    </View>
            </Modal>
            {/* delete modal */}
            <Modal 
                animationType="fade"
                visible={modalStateDelete}
                transparent={true}
                >
                <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

                    <TouchableWithoutFeedback>
                        <View style={styles.modalOverlay}/>
                    </TouchableWithoutFeedback> 
                    
                    <View style={styles.alertCard}>
                        {deleteAskState 
                        ?   <AlertCard 
                                cardTypeAsk={true}
                                cardImage={require('../Assets/Icons/QuestionMarkIcon.png')}
                                cardText="Are you sure want to delete this note?"
                                onPressButtonYes={() => handleDelete()}
                                onPressButtonNo={() => dispatch({type:'DELETE_MODAL_CLOSE'})}
                            /> 
                        : loadingDelete ? <LoadingCard />
                        : statusPost == 'success' ? <AlertCard 
                                                    cardTitle={true}
                                                    cardTitleText="Delete Note Successfully"
                                                    cardImage={require('../Assets/Icons/RoundCheckIcon.png')}
                                                    cardText="We have successfully delete your note"
                                                    buttonTitle="Ok"
                                                    onPressCardButton={() => {
                                                        dispatch({type: 'DELETE_SUCCESS_OK'})
                                                        props.navigation.navigate('AllNotesScreen')
                                                    }}
                                                />
                        : <AlertCard 
                            cardTitle={true}
                            cardTitleText="Delete Notes Failed"
                            cardImage={require('../Assets/Icons/RoundCrossIcon.png')}
                            cardText="Sorry, we couldn't delete your note right now"
                            buttonTitle="Ok"
                            onPressCardButton={() => dispatch({type:'DELETE_FAILED_OK'})}
                        />
                        }
                        
                    </View>
            </Modal>
            
        </View>
    )
}

export default NotesScreen

const styles = StyleSheet.create({
    backgroundBase : {
        width:'100%',
        height:'100%',
        backgroundColor: '#F1F4FA',
        alignItems:'center',
        paddingHorizontal:20,
    },
    bodyContainer : {
        width:'100%',
        height:400,
        alignItems:'center',
        marginVertical: 20,
    },
    colorPickerContainer : {
        width:'100%', 
        flexDirection:'row',
        justifyContent:'space-between',
    },
    modalOverlay : {
        flex:1,
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
        backgroundColor:'transparent'
    },
    alertCard : {
        position:'absolute',
        left:'15%',
        right:'15%',
        top:'25%',
        height:'40%',
    }
})
