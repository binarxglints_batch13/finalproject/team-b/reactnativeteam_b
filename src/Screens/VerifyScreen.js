import React, { useEffect } from 'react'
import { Image, Linking, StyleSheet, Text, View } from 'react-native'
import { PrimaryButtonLarge } from '../Components/PrimaryButton'
import globalStyle from '../Util/globalStyle'

const VerifyScreen = (props) => {
    useEffect(() => {
        Linking.addEventListener('url', e => {
        const token = e?.url.split('/')[4]
        console.log('e', token);
        });
        const getUrlAsync = async () => {
            const initialUrl = await Linking.getInitialURL();
            const token = initialUrl?.split('/')[4]
            console.log('initial URL ', initialUrl);
            console.log(token)
            };

        return getUrlAsync();
    }, []);
    
    return (
        <View style={{
            flex: 1,
            alignItems:'center', 
            backgroundColor:globalStyle.primaryColor,
            padding:20,
            justifyContent:"space-between"
            }}>
            <Text style={{fontFamily:'Poppins-Regular',color:globalStyle.TCdark, fontSize:30}}>Congratulation</Text>
            <View style={{width:250, height:250,}}>
                <Image source={require('../Assets/Images/Splash-2.png')} style={{width:250, height:250,}} />
            </View>
            <Text style={{fontFamily:'Poppins-Regular',color:globalStyle.TCdark , fontSize:20}}>You have successfully verified</Text>
            <PrimaryButtonLarge 
                title={'Great, Lets go login!'}
                onPress={() => props.navigation.navigate('Login')}
            />
        </View>
    )
}

export default VerifyScreen

const styles = StyleSheet.create({})
