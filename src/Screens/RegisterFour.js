import React, { useState } from 'react'
import { StyleSheet, Text, View, Dimensions, TouchableWithoutFeedback, Image, KeyboardAvoidingView, } from 'react-native'
import { useDispatch } from 'react-redux'
import { HeaderWithText } from '../Components/Header'
import {InputField} from '../Components/InputField'
import { OutlineButtonMed } from '../Components/OutlineButton'
import { PrimaryButtonMed } from '../Components/PrimaryButton'
import globalStyle from '../Util/globalStyle'

// validate email
const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const RegisterThree = (props) => {
    const dispatch = useDispatch()
    const [emailPlaceholder, setEmailPlaceholder] = useState('yourname@remindme.com')
    const [email, setEmail] = useState('')


    const handleNext = () => {
        if (email.match(mailformat)) {
            dispatch({type:'EMAIL', email: email})
            console.log('email pushed')
            props.navigation.navigate('RegisPageFive')    
        } else {
            alert('Invalid email')
        }
    }
    // const handleNext = () => {
    //     email.match(mailformat) 
    //     ? props.navigation.navigate('RegisPageFive')
    //     : alert('Invalid email')
    // }


    return (
        <KeyboardAvoidingView behavior="position" style={{flex:1}}>
        <View style={styles.screenConfig}>

            <HeaderWithText 
                onPressBack={() => props.navigation.navigate('RegisPageThree')}
                title='Sign Up'
            />

            <View style={{width:'100%'}}>
                <Text style={styles.sectionText}>E-mail</Text>
                <Text style={styles.sectionSubText}>Tell use how to reach you by enter your email address in the box below.</Text>
            </View>
            <Image source={require('../Assets/Images/Register-4.png')} style={{width:'100%', height:'40%', marginVertical:10}}/>
            
            <View style={{width:'100%', alignItems:'center'}}>
                <InputField 
                    title={emailPlaceholder}
                    onFocus={() => setEmailPlaceholder('')}
                    onBlur={() => setEmailPlaceholder('yourname@remindme.com')}
                    onChangeText={(text) => setEmail(text)}
                    value={email}
                    />
            </View>

            {email === '' ? null : email.match(mailformat) 
            ? <Text style={{color:'green'}}>This is a valid email</Text> 
            : <Text style={{color:'red'}}>This is an invalid email</Text>
            }

            <View style={{
                width:'100%', 
                marginVertical:40, 
                alignItems:'center', 
                flexDirection:"row", 
                justifyContent:'space-evenly'
                }}>
                <OutlineButtonMed 
                title="Previous"
                containerStyle={{marginHorizontal:20}}
                onPress={() => props.navigation.navigate('RegisPageThree')}
                />
                <PrimaryButtonMed 
                    title="Next"
                    containerStyle={{marginHorizontal:20}}
                    onPress={() => handleNext()}
                />
            </View>

            
        </View>
        </KeyboardAvoidingView>
    )
}

export default RegisterThree

const styles = StyleSheet.create({
    screenConfig: {
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
        alignItems:'center', 
        backgroundColor:globalStyle.primaryColor,
        paddingHorizontal:20
    },
    sectionText : {
        fontFamily: "Poppins-medium",
        fontSize:24,
        color:globalStyle.TCdark
    },
    sectionSubText : {
        fontFamily: "Poppins-ExtraLight", 
        fontSize:16, 
        color:globalStyle.TCdark
    }
})
