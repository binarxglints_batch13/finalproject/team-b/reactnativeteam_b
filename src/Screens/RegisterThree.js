import React, { useState } from 'react'
import { StyleSheet, Text, View, Dimensions, TouchableWithoutFeedback, Image, KeyboardAvoidingView, } from 'react-native'
import { useDispatch } from 'react-redux'
import { HeaderWithText } from '../Components/Header'
import {InputField} from '../Components/InputField'
import { OutlineButtonMed } from '../Components/OutlineButton'
import { PrimaryButtonMed } from '../Components/PrimaryButton'
import globalStyle from '../Util/globalStyle'

const RegisterThree = (props) => {
    const dispatch = useDispatch()
    const [usernamePlaceholder, setUsernamePlaceholder] = useState('Username')
    const [username, setUsername] = useState('')
    console.log(username)

    const handleNext = () => {
        if (username !== '') {
            dispatch({type:'USERNAME', userName: username})
            console.log('username pushed')
            props.navigation.navigate('RegisPageFour')    
        } else {
            alert('Username must not empty')
        }
    }

    return (
        <KeyboardAvoidingView behavior="position" style={{flex:1}}>
        <View style={styles.screenConfig}>

            <HeaderWithText 
                onPressBack={() => props.navigation.navigate('RegisPageTwo')}
                title='Sign Up'
            />

            <View style={{width:'100%'}}>
                <Text style={styles.sectionText}>Username</Text>
                <Text style={styles.sectionSubText}>What should we call you? please enter your username.</Text>
            </View>

            <Image source={require('../Assets/Images/Register-3.png')} style={{width:'100%', height:'40%', marginVertical:10}}/>
            
            <View style={{width:'100%', alignItems:'center'}}>
                <InputField 
                    title={usernamePlaceholder}
                    onFocus={() => setUsernamePlaceholder('')}
                    onBlur={() => setUsernamePlaceholder('Username')}
                    onChangeText={(text) => setUsername(text)}
                    value={username}
                    />
            </View>

            <View style={{
                width:'100%', 
                marginVertical:40, 
                alignItems:'center', 
                flexDirection:"row", 
                justifyContent:'space-evenly'
                }}>
                <OutlineButtonMed 
                    title="Previous"
                    containerStyle={{marginHorizontal:20}}
                    onPress={() => props.navigation.navigate('RegisPageTwo')}
                />
                <PrimaryButtonMed 
                    title="Next"
                    containerStyle={{marginHorizontal:20}}
                    onPress={() => handleNext()}
                />
            </View>

            
        </View>
        </KeyboardAvoidingView>
    )
}

export default RegisterThree

const styles = StyleSheet.create({
    screenConfig: {
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
        alignItems:'center', 
        backgroundColor:globalStyle.primaryColor,
        paddingHorizontal:20
    },
    sectionText : {
        fontFamily: "Poppins-medium",
        fontSize:24,
        color:globalStyle.TCdark
    },
    sectionSubText : {
        fontFamily: "Poppins-ExtraLight", 
        fontSize:16, 
        color:globalStyle.TCdark
    }
})
