import React, { useState } from 'react'
import { StyleSheet, Text, View, Dimensions, Touchable, TouchableWithoutFeedback, Image, TouchableOpacity, KeyboardAvoidingView } from 'react-native'
import { useDispatch } from 'react-redux'
import { HeaderWithText } from '../Components/Header'
import {InputField} from '../Components/InputField'
import { PrimaryButtonLarge } from '../Components/PrimaryButton'
import globalStyle from '../Util/globalStyle'

const RegisterOne = (props) => {
    const dispatch = useDispatch()
    const [firstnamePlaceholder, setFirstnamePlaceholder] = useState('First Name')
    const [firstname, setFirstname] = useState('')
    // console.log('firstname',firstname)


    const handleNext = () => {
        if (firstname !== '') {
            dispatch({type:'FIRST_NAME', firstName: firstname})
            console.log('firstname pushed')
            props.navigation.navigate('RegisPageTwo')    
        } else {
            alert('Firstname must not empty')
        }
    }

    return (
        <KeyboardAvoidingView behavior="position" style={{flex:1}}>
            <View style={styles.screenConfig}>
                <HeaderWithText 
                    onPressBack={() => props.navigation.navigate('Login')}
                    title='Sign Up'
                />
                
                <View style={{width:'100%'}}>
                    <Text style={styles.sectionText}>First Name</Text>
                    <Text style={styles.sectionSubText}>The first step to start is enter your first name in this box below.</Text>
                </View>

                <Image source={require('../Assets/Images/Register-1.png')} style={{width:'100%', height:'40%', marginVertical:10}}/>
                
                <View style={{width:'100%', alignItems:'center'}}>
                    <InputField 
                        title={firstnamePlaceholder}
                        onFocus={() => setFirstnamePlaceholder('')}
                        onBlur={() => setFirstnamePlaceholder('First Name')}
                        onChangeText={(text) => setFirstname(text)}
                        value={firstname}
                        />
                </View>

                <View style={{width:'100%', marginVertical:40, alignItems:'center'}}>
                    <PrimaryButtonLarge 
                        title="Next"
                        onPress={() => handleNext()}
                    />
                </View>
                
            </View>
        </KeyboardAvoidingView>
    )
}

export default RegisterOne

const styles = StyleSheet.create({
    screenConfig: {
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
        alignItems:'center', 
        backgroundColor: globalStyle.primaryColor,
        paddingHorizontal:20
    },
    signUpText: {
        fontFamily: "Poppins-Regular", 
        fontSize:18, 
        alignSelf:'center',
        color:globalStyle.TCdark
    },
    sectionText : {
        fontFamily: "Poppins-medium",
        fontSize:24,
        color:globalStyle.TCdark
    },
    sectionSubText : {
        fontFamily: "Poppins-ExtraLight", 
        fontSize:16, 
        color:globalStyle.TCdark
    }
})
