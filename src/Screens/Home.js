import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, ActivityIndicator, Dimensions} from 'react-native'
import CircularProgress from 'react-native-circular-progress-indicator'
import {useDispatch, useSelector} from 'react-redux'
import GoalsItem from '../Components/GoalsItem'

import {HomeModal} from '../Components/Modal'
import NotesItem from '../Components/NotesItem'
import { PrimaryButtonLarge } from '../Components/PrimaryButton'
import TimelineItem from '../Components/TimelineItem'
import WeekCalendarComponents from '../Components/WeekCalendarComponents'
import globalStyle from '../Util/globalStyle'

const todayDate = moment.utc().format('YYYY-MM-DD');

const Home = (props) => {
    const dispatch = useDispatch()
    const modalState = useSelector(state => state.modal.modalState)
    
    const userData = useSelector(state => state.user.userData)
    const goalsData = useSelector(state => state.goals.goalsData)
    const notesData = useSelector(state => state.notes.notesData)
    const notesDataByDate = useSelector(state => state.notes.notesDatabyDate)
    // console.log(notesData)
    const isLoadingGoals = useSelector(state => state.goals.isLoadingData)
    const isLoadingUser = useSelector(state => state.user.isLoading)
    const isLoadingDataDate = useSelector(state => state.notes.isLoadingDataDate)
    console.log('INI DATA GOALS', goalsData)
    // console.log('INI USERDATA', userData)
    console.log('INI DATA Note', notesData)

    const goalsDataFilter = typeof goalsData == 'undefined' ? null :goalsData?.sort((a,b) => a.current_percent - b.current_percent)
    // console.log('filtere',goalsDataFilter)
    //notes pinned data
    const notesDataPinned = typeof notesData == 'undefined' ? [] : notesData?.filter((item) => item.pinned === true)
    // console.log("pinned data", notesDataPinned)
    // console.log("SORT BY DATE", sortDateData)

    useEffect(() => {
        dispatch({type: 'GET_USER_DATA'})
        dispatch({type: 'GET_DATA_GOALS'})
        dispatch({type: 'GET_NOTES_DATA'})
        dispatch({type:'GET_NOTES_BY_DATE', date: todayDate})
    }, [])

    const navigateDetails = async (item) => {
        await dispatch({type:'GET_GOALS_DETAIL_BY_ID', goalsId: item.id})
        props.navigation.navigate('GoalsDetailScreen', {goalsId: item.id})
    }

    const navigateAllNotes = () => {
        dispatch({type:'GET_NOTES_DATA'})
        props.navigation.navigate('AllNotesScreen')
    }
    const navigateNotesDetails = (item) => {
        dispatch({type: 'GET_NOTES_DETAIL', notesId: item.id})
        dispatch({type:'READ_STATE'})
        props.navigation.navigate('NotesScreen', {notesDetailData: item} )
    }

    const dotedote =  () => {
        let newObject = notesData.reduce((prev, item) => ({
            ...prev, [moment.utc(item.dateNote).format('Y-MM-DD')]: {dots: []}
        }), {})
        for(let item of notesData) {
            let date = moment.utc(item.dateNote).format('Y-MM-DD') 
            if(newObject.hasOwnProperty(moment.utc(item.dateNote).format('Y-MM-DD'))) {
                newObject[`${date}`]['dots'].push({'key':item.id, 'color': item.color})
            }
        }
        return newObject
    }

    const renderGoalItem = ({item}) => {
        return(
            <GoalsItem 
                onPress={() => navigateDetails(item)}
                itemColor={item.color}
                itemName={item.name}
                percent={item.current_percent}
            />

        )
    }
    
    const renderNotesItem = ({item}) => {
        return(
            <NotesItem 
                noteColor={item.color}
                title={item.title}
                bodyNote={item.body}
                onPress={() => navigateNotesDetails(item)}
            />
        )
    }

    const renderTimeline = ({item}) => {
        return(
            <TimelineItem 
                title={item.title}
                color={item.color}
                body={item.body}
                dateNote={item.dateNote}
                onPress={() => navigateNotesDetails(item)}
            />
        )
    }


    if(isLoadingUser) {
        return (
            <ActivityIndicator size="large" color="blue" style={{flex:1, backgroundColor:globalStyle.primaryColor}}/>
        )
    }

    return (
        <View style={{flex:1, alignItems:'center', backgroundColor:globalStyle.primaryColor}}>
            <View style={{width:'100%', paddingHorizontal:20, flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                <View style={{flexDirection:'row', justifyContent:'center'}}>
                    <Image source={require('../Assets/Icons/RemindMe_icon.png')} 
                    style={{width:24, height:24}}
                    />
                    <Text style={{fontFamily:'Poppins-Medium', fontSize:16, marginHorizontal:10}}>
                        Hi, {`${userData.firstname} ${userData.lastname}`}
                    </Text>
                </View>
                <Image source={{uri: userData.image}} style={{width:45, height:45, borderRadius:50}} />
            </View>

            <View style={{width:"100%", marginTop:10}}>
                <WeekCalendarComponents 
                    markedDates={typeof notesData == 'undefined' ? {} : dotedote()}
                    onPressCalendar={() => props.navigation.navigate('CalendarScreen')}
                />
            </View>
            {/* render item */}
            {isLoadingGoals ? <ActivityIndicator size="large" color="blue" />
            : goalsData.length < 1 && typeof notesData == 'undefined'
            ? <View style={{width:'100%', alignItems:'center',paddingHorizontal:20}}>
                <Image source={require('../Assets/Images/borderline.png')} style={{width:'100%', }}/>
                <Image source={require('../Assets/Images/LogoWelcome.png')} style={{width:'100%', height:'80%'}}/>
            </View> 
            : <View style={{flex:1,width:'100%'}}>
                <View style={{flexDirection:'row', width:'100%', paddingHorizontal:20, justifyContent:'space-between'}}>
                    <Text style={{fontFamily:'Poppins-Regular'}}>Pinned Notes</Text>
                    <TouchableOpacity onPress={() => navigateAllNotes()}>
                        <Text style={{fontFamily:'Poppins-Regular',textDecorationLine:'underline', color:globalStyle.TCdark}}>See All Notes</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex:1, width:'100%', paddingLeft:20,}}>
                    <FlatList 
                        data={typeof notesDataPinned == 'undefined' ? null : notesDataPinned}
                        renderItem={renderNotesItem}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        extraData={notesDataPinned}
                        
                        ListEmptyComponent={() => {
                            return(
                                <View style={{width:Dimensions.get('screen').width, justifyContent:'center', alignItems:'center'}}>
                                    <Text>No Pinned Notes</Text>
                                </View>
                            )
                        }}
                    />
                </View>
                <View style={{flex:1, width:'100%',paddingLeft:20}}>
                    <Text style={{fontFamily:'Poppins-Regular'}}>Daily Streak</Text>
                    <FlatList 
                        data={goalsData}
                        renderItem={renderGoalItem}
                        horizontal={true}
                        extraData={goalsData}
                        showsHorizontalScrollIndicator={false}
                        ListEmptyComponent={() => {
                            return(
                                <View style={{width:Dimensions.get('screen').width, justifyContent:'center', alignItems:'center'}}>
                                    <Text>No Goals </Text>
                                </View>
                                )
                        }}
                    />
                </View>
                <View style={{flex:1, width:'100%', paddingHorizontal:20}}>
                    <Text style={{fontFamily:'Poppins-Regular'}}>Today's Timeline</Text>
                    {isLoadingDataDate ? <ActivityIndicator size="large" color="blue" style={{flex:1, backgroundColor:globalStyle.primaryColor}}/>
                    :   <FlatList 
                        data={notesDataByDate?.length == 0 ? null : notesDataByDate}
                        renderItem={renderTimeline}
                        extraData={notesDataByDate}
                        showsVerticalScrollIndicator={false}
                        ListEmptyComponent={() => {
                            return(
                                <View style={{
                                    width:Dimensions.get('screen').width, 
                                    justifyContent:'center', 
                                    alignItems:'center',
                                    height:100,
                                    }}>
                                    <Text>No timeline for today</Text>
                                </View>
                            )
                        }}
                    />
                    }
                    
                </View>
            </View>
            }
            
            

            
            
            <HomeModal 
                modalVisible={modalState} 
                toNotes={() => {
                    dispatch({type: 'CLOSE_MODAL'})
                    dispatch({type:'EDIT_STATE'})
                    props.navigation.navigate('NotesScreen')
                }}
                toGoals={() => {
                    dispatch({type: 'CLOSE_MODAL'})
                    props.navigation.navigate('GoalsScreen')
                }}
            />
        </View>
    )
}

export default Home

const styles = StyleSheet.create({})
