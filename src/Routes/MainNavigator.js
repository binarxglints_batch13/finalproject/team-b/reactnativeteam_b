import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Icon from 'react-native-vector-icons/Entypo'
import IconFontAwesome from 'react-native-vector-icons/FontAwesome5'


import Profile from '../Screens/Profile'
import AddButton from '../Components/AddButton'
import Home from '../Screens/Home'

const Tab = createBottomTabNavigator()


const MainNavigator = (props) => {
    return (
            <Tab.Navigator initialRouteName="Dashboard" screenOptions={{
                headerShown: false,
                tabBarStyle : {
                    backgroundColor:'#B6C6E5'
                },
                tabBarShowLabel:false
            }}>
                <Tab.Screen 
                    name="Dashboard" 
                    component={Home}
                    options={{
                        tabBarIcon: ({focused}) => (
                            <Icon name="home" color={focused ? '#625BAD' : '#F1F4FA'}  size={24} />
                        ),
                    }}
                />
                <Tab.Screen name="add" options={{
                    tabBarIcon : ({focused}) => (
                        <AddButton />
                    )
                    ,tabBarItemStyle :{
                        flex:0,
                        width:50,
                        height:40,
                        elevation:10,
                        borderRadius:100
                    }
                }}>
                    {() => null}
                </Tab.Screen>
                <Tab.Screen 
                    name="Profile" 
                    component={Profile}
                    options={{
                        tabBarIcon: ({focused}) => (
                            <IconFontAwesome name="user-alt" color={focused ? '#625BAD' : '#F1F4FA'}  size={24} />
                        ),
                    }}
                />
            </Tab.Navigator>
    )
}

export default MainNavigator

const styles = StyleSheet.create({})
