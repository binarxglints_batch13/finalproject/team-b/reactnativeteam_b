import React from 'react'
import { View, Text } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import RegisterOne from '../Screens/RegisterOne'
import RegisterTwo from '../Screens/RegisterTwo'
import RegisterThree from '../Screens/RegisterThree'
import RegisterFour from '../Screens/RegisterFour'
import RegisterFive from '../Screens/RegisterFive'

const Stack = createStackNavigator()

const RegisterStack = () => {
    return (
        <Stack.Navigator initialRouteName="RegisPageOne" screenOptions={{
            headerShown:false,
        }}>
            <Stack.Screen name="RegisPageOne" component={RegisterOne} />
            <Stack.Screen name="RegisPageTwo" component={RegisterTwo} />
            <Stack.Screen name="RegisPageThree" component={RegisterThree} />
            <Stack.Screen name="RegisPageFour" component={RegisterFour} />
            <Stack.Screen name="RegisPageFive" component={RegisterFive} />
        </Stack.Navigator>
    )
}

export default RegisterStack
