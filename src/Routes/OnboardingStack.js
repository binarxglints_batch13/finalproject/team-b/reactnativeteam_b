import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import LoginScreen from '../Screens/LoginScreen'
import { NavigationContainer } from '@react-navigation/native'
import RegisterStack from './RegisterStack'
import LandingPage from '../Screens/LandingPage'
import VerifyScreen from '../Screens/VerifyScreen'
import ForgotScreen from '../Screens/ForgotScreen'

const Stack = createStackNavigator()

const OnboardingStack = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown:false
        }}>
            <Stack.Screen name="LandingPage" component={LandingPage} />
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen name="Register" component={RegisterStack} />
            <Stack.Screen name='Verify' component={VerifyScreen} />
            <Stack.Screen name='Forgot' component={ForgotScreen} />
        </Stack.Navigator>
    )
}

export default OnboardingStack

const styles = StyleSheet.create({})
