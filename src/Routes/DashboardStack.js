import React, { useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import NotesScreen from '../Screens/NotesScreen'
import { getFocusedRouteNameFromRoute } from '@react-navigation/core'
import MainNavigator from './MainNavigator'
import GoalsScreen from '../Screens/GoalsScreen'
import AllNotesScreen from '../Screens/AllNotesScreen'
import GoalsDetailScreen from '../Screens/GoalsDetailScreen'
import CalendarScreen from '../Screens/CalendarScreen'
import LandingPage from '../Screens/LandingPage'

const Stack = createStackNavigator()

const DashboardStack = ({route, navigation}) => {
    // useEffect(() => {
    //     let focusedStack = getFocusedRouteNameFromRoute(route);
    //     if (focusedStack === 'NotesScreen') {
    //       navigation.setOptions({tabBarVisible: false});
    //     } else {
    //       navigation.setOptions({tabBarVisible: true});
    //     }
    //   }, [route]);

    return (
        <Stack.Navigator screenOptions={{
            headerShown:false
        }}>
            <Stack.Screen name="Home" component={MainNavigator} />
            <Stack.Screen name="NotesScreen" component={NotesScreen}/>
            <Stack.Screen name="GoalsScreen" component={GoalsScreen} />
            <Stack.Screen name="AllNotesScreen" component={AllNotesScreen} />
            <Stack.Screen name="GoalsDetailScreen" component={GoalsDetailScreen} />
            <Stack.Screen name="CalendarScreen" component={CalendarScreen} />   
        </Stack.Navigator>
    )
}

export default DashboardStack

const styles = StyleSheet.create({})
