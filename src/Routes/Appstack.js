import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import SplashScreen from 'react-native-splash-screen'

import OnboardingStack from './OnboardingStack'
import DashboardStack from './DashboardStack'
import { useDispatch, useSelector } from 'react-redux'

import { getToken } from '../Util/authAsyncStorage'

const Stack = createStackNavigator()

const Appstack = () => {
    const dispatch = useDispatch()
    const verify = useSelector(state => state.auth.isLoggedIn)
    const [token, setToken] = useState(null)
    
    // console.log(token)
    useEffect(async () => {
        const tempToken = await getToken();
        if (tempToken) {
          setToken(tempToken);
          dispatch({type:'VERIFY_TOKEN'})
          // console.log(tempToken)
        } else {
          setToken(null)
        }
        // console.log(token)
        // dispatch({type: 'GET_DATA'});
        // dispatch({type: 'GET_USER'});
        setTimeout(() => {
          SplashScreen.hide();
        }, 3000);
        // await getToken();
      }, []);

    return (
        <Stack.Navigator screenOptions={{
            headerShown:false
        }}>
            {verify ? (<Stack.Screen name="MainNavigator" component={DashboardStack} />) 
                    : <Stack.Screen name="OnboardingStack" component={OnboardingStack} />

            }
            {/* <Stack.Screen name="OnboardingStack" component={OnboardingStack} />
            <Stack.Screen name="MainNavigator" component={DashboardStack} /> */}
        </Stack.Navigator>
    )
}

export default Appstack

const styles = StyleSheet.create({})
