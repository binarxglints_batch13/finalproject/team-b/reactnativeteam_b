import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Calendar } from 'react-native-calendars'
import moment from 'moment'

// const today = moment().format('Y-MM-D')

const MonthlyCalendarComponent = (props) => {
    // const [selected, setSelected] = useState(today)

    return (
        <View style={{width:"100%"}}>
            <Calendar
                onDayPress={props.onDayPress
                //     (day) => 
                //     {console.log('selected day', day)
                //     setSelected(day.dateString)
                // }
                }
                hideArrows={props.hideArrows}
                enableSwipeMonths={props.enableSwipeMonths}
                disableArrowLeft={props.disableArrowLeft}
                disableArrowRight={props.disableArrowRight}
                markingType={props.markingType}
                markedDates={props.markedDates
                    // {
                //     [props.selected]: {
                //       selected: true,
                //       selectedColor: '#625BAD',
                //       selectedTextColor: '#F1F4FA'
                //     }
                //   }
                }
                hideExtraDays={props.hideExtraDays}
                theme={props.theme}
                    // {
                //     backgroundColor:"#B6C6E5",
                //     calendarBackground:'#B6C6E5',
                //     textSectionTitleColor:'#342D50',
                //     textDayFontFamily: 'Poppins-Regular',
                //     textDayHeaderFontFamily: 'Poppins-Regular',
                //     disabledArrowColor:'#B6C6E5',
                //     textDayFontSize:12,
                //     textDayHeaderFontSize:12,
                //     textDisabledColor: 'rgba(52, 45, 80, 0.5)',
                //     'stylesheet.calendar.header' : {
                //         monthText : {
                //             fontSize: 13,
                //             width:'100%',
                //             alignSelf: 'flex-start',
                //             marginVertical:10,
                //             fontFamily:'Poppins-Regular',
                //             color:'#342D50'
                //         },
                //     },
                //     'stylesheet.calendar.main' : {
                //         week: {
                //             flexDirection: 'row',
                //             justifyContent: 'space-around'
                //           },
                //     }
                // }}
                
            />
        </View>
    )
}

export default MonthlyCalendarComponent

const styles = StyleSheet.create({})
