import React from 'react'
import { StyleSheet, Text, Image, TouchableOpacity, View } from 'react-native'

const RoundButton = (props) => {
    return (
        <View style={{justifyContent:'center', alignItems:'center'}}>
            <TouchableOpacity onPress={() => props.onPress()}>
                <View style={{width: 40, height: 40, backgroundColor: props.backgroundColor, justifyContent:'center', alignItems:'center', borderRadius:20}} >
                    <Image source={props.image} style={{width:30, height:30}}/>
                </View>
            </TouchableOpacity>
            <Text style={{fontFamily:'Poppins'}}>{props.title}</Text>
        </View>
    )
}

export default RoundButton

const styles = StyleSheet.create({})
