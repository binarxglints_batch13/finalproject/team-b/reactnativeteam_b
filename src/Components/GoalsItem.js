import React from 'react'
import { Pressable, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import CircularProgress from 'react-native-circular-progress-indicator'
import globalStyle from '../Util/globalStyle'

const GoalsItem = (props) => {
    return(
        <View style={{width:80,height:100,alignItems:'center'}}>
        
        <TouchableOpacity 
            onPress={props.onPress} 
            style={{ width:'100%', height:'100%', zIndex:2, position:'absolute'}}
            />
            <View style={{
                width:60,
                height:60,
                borderRadius: 100, 
                alignItems:'center',
                position:'absolute',
                top:6,
                left:10,
                backgroundColor:props.itemColor
            }}/>
            
            <CircularProgress 
                value={props.percent}
                valueSuffix={'%'}
                radius={35}
                textStyle={{fontSize:'Poppins-Regular'}}
                textColor={globalStyle.primaryColor}
                activeStrokeColor={props.itemColor === globalStyle.CCpink ? globalStyle.strokeColorPink //pink
                                    : props.itemColor === globalStyle.CClemon ? globalStyle.strokeColorLemon //lemon
                                    : props.itemColor === globalStyle.CCgreen ? globalStyle.strokeColorGreen//green
                                    : props.itemColor === globalStyle.CCred ? globalStyle.strokeColorRed //red
                                    : props.itemColor === globalStyle.CCpurple ? globalStyle.strokeColorPurple //purple
                                    : null
                                    }
                inActiveStrokeColor={globalStyle.primaryColor}
                inActiveStrokeWidth={5}
                activeStrokeWidth={5}
            />
            
            
            <Text style={{fontFamily:'Poppins-Regular', }}>{props.itemName}</Text>
            
        </View>
    )
}

export default GoalsItem

const styles = StyleSheet.create({})
