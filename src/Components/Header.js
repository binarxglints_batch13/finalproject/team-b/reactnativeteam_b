import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import { useDispatch } from 'react-redux'

export const HeaderWithText = (props) => {
    return (
        <View style={{...styles.headerContainer, ...props.headerContainerStyle}}>
            <View style={{flex:1}}>
                <TouchableWithoutFeedback onPress={() => props.onPressBack()}>
                    <Image source={require('../Assets/Icons/BackArrow.png')} style={{width:50, height:50}}/>
                </TouchableWithoutFeedback>
            </View>
            <View style={{width:'100%', justifyContent:'center'}}>
                <Text style={styles.sectionTitle}>{props.title}</Text>
            </View>
        </View>
    )
}

export const HeaderWithIcons = (props) => {
    const dispatch = useDispatch()

    return(
        <View style={{...styles.headerContainer, justifyContent:'flex-end'}}>
            <View style={{flex:1}}>
                <TouchableWithoutFeedback onPress={() => props.onPressBack()}>
                    <Image source={require('../Assets/Icons/BackArrow.png')} style={{width:50, height:50}}/>
                </TouchableWithoutFeedback>
            </View>
            <View style={styles.iconContainer}>
                {props.pinIcon === true 
                ? <TouchableWithoutFeedback onPress={props.onPressPin}>
                    <Image source={require('../Assets/Icons/PinnedNotes_light.png')} style={{...styles.iconsSize, tintColor:props.pinStyle}}/>
                </TouchableWithoutFeedback> : null
                }
                {props.calendarIcon === true 
                ? <TouchableOpacity onPress={() => dispatch({type: 'OPEN_MODAL_CALENDAR'})}>
                    <Image source={require('../Assets/Icons/CalendarIcon.png')} style={styles.iconsSize}/>
                </TouchableOpacity> : null
                }
                {props.imageIcon === true 
                ? <TouchableOpacity onPress={props.onPressImage}>
                    <Image source={require('../Assets/Icons/AddImageIcon.png')} style={styles.iconsSize}/>
                </TouchableOpacity> : null
                }
                {props.deleteIcon === true 
                ? <TouchableOpacity onPress={props.onPressDelete}>
                    <Image source={require('../Assets/Icons/deleteIcon.png')} 
                    style={{...styles.iconsSize, width:16, height:20, marginLeft:50}}/>
                </TouchableOpacity> : null
                }
            </View>
        </View>
    )
}

export const HeaderWithEdit = (props) => {
    return (
        <View style={{...styles.headerContainer, justifyContent:'space-between'}}>
            <View style={{flex:1/3}}>
                <TouchableWithoutFeedback onPress={() => props.onPressBack()}>
                    <Image source={require('../Assets/Icons/BackArrow.png')} style={{width:50, height:50}}/>
                </TouchableWithoutFeedback>
            </View>
            <View style={{flex:1,justifyContent:'center'}}>
                <Text style={styles.sectionTitle}>{props.title}</Text>
            </View>
            <TouchableOpacity onPress={props.onPressEdit} style={{flex:1/3, justifyContent:'center', alignItems:'center'}}>
                <Text style={styles.sectionTitle}>Edit</Text>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    headerContainer : {
        flexDirection:'row', 
        width:'100%',
        height: 60,
        alignContent: 'center',
        justifyContent:'center',
    },
    sectionTitle: {
        fontFamily: "Poppins-Regular", 
        fontSize:18, 
        alignSelf:'center',
        color:'#342D50'
    },
    iconContainer : {
        width:'40%', 
        justifyContent:'center',
        alignItems:"center", 
        flexDirection: 'row',
    },
    iconsSize : {
        width: 20, 
        height:20,
        alignSelf:'center',
        marginHorizontal:10,
        marginVertical: 10
    }
})
