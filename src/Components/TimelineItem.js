import moment from 'moment'
import React from 'react'
import { Dimensions, StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const TimelineItem = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress} style={{width:'100%', justifyContent:'center'}}>
            <View style={{flexDirection:'row', marginVertical:10, justifyContent:'space-between'}}>
                <Text style={{paddingVertical:10, fontSize:12, fontFamily:'Poppins-Regular'}}>{moment.utc(props.dateNote).format('HH.mm A')}</Text>
                <View style={{width:250, height:60, backgroundColor:props.color, padding:10, paddingHorizontal:20, elevation:4, borderRadius:10}}>
                    <Text style={{fontFamily:'Poppins-Medium'}} numberOfLines={1}>{props.title}</Text>
                    <Text style={{fontFamily:'Poppins-Thin'}} numberOfLines={1}>{props.body}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default TimelineItem

const styles = StyleSheet.create({})
