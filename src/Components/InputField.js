import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'

export const InputField = (props) => {


    return (
        <View style={[styles.container, props.style]}>
            <TextInput
            ref={props.ref}
            placeholder={props.title}
            onFocus={props.onFocus}
            onBlur={props.onBlur}
            onChangeText={props.onChangeText}
            value={props.value}
            style={styles.field}
            caretHidden={props.caretHidden}
            editable={props.editable}
            keyboardType={props.keyboardType}
            onSubmitEditing={props.onSubmitEditing}
            blurOnSubmit={props.blurOnSubmit}
           />
        </View>
    )
}

export const ProfileInputField = (props) => {


    return (
        <View style={props.hideBorder === true ? {...styles.container, borderWidth:0, paddingHorizontal:10} : {...styles.container, paddingHorizontal:10}}>
            <TextInput 
            placeholder={props.title}
            onFocus={props.onFocus}
            onBlur={props.onBlur}
            onChangeText={props.onChangeText}
            value={props.value}
            style={styles.field}
            caretHidden={props.caretHidden}
            editable={props.editable}
           />
        </View>
    )
}

export const SmallInputField = (props) => {


    return (
        <View style={styles.containerSmall}>
            <TextInput 
            placeholder={props.title}
            onFocus={props.onFocus}
            onBlur={props.onBlur}
            onChangeText={props.onChangeText}
            value={props.value}
            style={{...styles.field, textAlign:'center'}}
            caretHidden={props.caretHidden}
            editable={props.editable}
            keyboardType={props.keyboardType}
            onSubmitEditing={props.onSubmitEditing}
            maxLength={props.maxLength}
           />
        </View>
    )
}

const styles = StyleSheet.create({
    container : {
        width:'100%', 
        height:40, 
        borderWidth:1, 
        borderRadius: 10,
        borderColor:'#B6C6E5',
        justifyContent:'center',
        paddingHorizontal:20
    },
    field : {
        width:"80%",  
        paddingVertical:0,
        color:'#342D50',
        fontFamily: "Poppins-Regular"
    },
    containerSmall : {
        flex: 1/2,
        height:40, 
        borderWidth:1, 
        borderRadius: 10,
        borderColor:'#B6C6E5',
        justifyContent:'center',
        paddingHorizontal:20,
        marginHorizontal:5
    },
})
