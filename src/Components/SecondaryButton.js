import React from 'react'
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'


// ======================== Secondary Button Large =============================
export const SecondaryButtonLarge = (props) => {
    return (
        <TouchableOpacity 
        style={{...styles.container, ...props.containerStyle}}
        onPress={props.onPress}>
            <Text style={{...styles.buttonText,...props.textStyle}}>{props.title}</Text>
        </TouchableOpacity>
    )
}


// ======================== Secondary Button Med =============================
export const SecondaryButtonMed = (props) => {
    return(
        <TouchableOpacity 
            style={{...styles.containerMed, ...props.containerStyle}}
            onPress={props.onPress}>
                <Text style={{...styles.buttonText, ...props.textStyle}}>{props.title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container : {
        width: "100%",
        height: 50,
        backgroundColor:'#B6C6E5',
        borderRadius: 100,
        alignItems:'center',
        justifyContent:'center',
        marginVertical:10,
    },
    containerMed : {
        width: "50%",
        height: 50,
        backgroundColor:'#B6C6E5',
        borderRadius: 100,
        alignItems:'center',
        justifyContent:'center',
    },
    buttonText : {
        color:'#F1F4FA', 
        fontFamily: "Poppins-SemiBold", 
        fontSize:20
    }
})
