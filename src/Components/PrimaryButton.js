import React from 'react'
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import globalStyle from '../Util/globalStyle'

// ======================== Primary Button Large =============================
export const PrimaryButtonLarge = (props) => {
    return (
        <TouchableOpacity 
        style={{...styles.container, ...props.containerStyle}}
        onPress={props.onPress}>
            <Text style={{...styles.buttonText,...props.textStyle}}>{props.title}</Text>
        </TouchableOpacity>
    )
}


// ======================== Primary Button Med =============================
export const PrimaryButtonMed = (props) => {
    return(
        <TouchableOpacity 
            style={{...styles.containerMed, ...props.containerStyle}}
            onPress={props.onPress}>
                <Text style={{...styles.buttonText, ...props.textStyle}}>{props.title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container : {
        width: "100%",
        height: 50,
        backgroundColor:globalStyle.secondaryColorDark,
        borderRadius: 100,
        alignItems:'center',
        justifyContent:'center',
    },
    containerMed : {
        width: "50%",
        height: 50,
        backgroundColor:globalStyle.secondaryColorDark,
        borderRadius: 100,
        alignItems:'center',
        justifyContent:'center',
    },
    buttonText : {
        color:globalStyle.TClight, 
        fontFamily: "Poppins-SemiBold", 
        fontSize:20
    }
})
