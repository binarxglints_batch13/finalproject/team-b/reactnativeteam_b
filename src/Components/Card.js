import React, { useState } from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import { OutlineButtonMed } from './OutlineButton'
import { PrimaryButtonLarge, PrimaryButtonMed } from './PrimaryButton'
import moment from 'moment'
import { InputField, SmallInputField } from './InputField'
import PasswordField from './PasswordField'
import globalStyle from '../Util/globalStyle'


// ======================== Alert Card =============================
export const AlertCard = (props) => {
    return (
        <View style={{...styles.cardContainer, ...props.cardStyle}}>
            {props.cardTitle === true 
            ? (
                <Text style={{fontFamily:'Poppins-Regular', fontSize:18}}>{props.cardTitleText}</Text>
            ) : null
            }
            <Image source={props.cardImage} style={{width:40, height:40}} />
            <Text style={{fontSize:14, fontFamily:'Poppins-Regular', textAlign:"center"}}>{props.cardText}</Text>
            {props.cardTypeAsk === true 
            ? (
                <View style={{flexDirection:'row', width:'100%', alignItems:"center", justifyContent:'space-between'}}>
                    <OutlineButtonMed 
                        title='Yes'
                        onPress={props.onPressButtonYes}
                        containerStyle={{width:100, height:30}}
                        textStyle={{fontSize:14}}
                    />
                    <PrimaryButtonMed 
                        title='No'
                        onPress={props.onPressButtonNo}
                        containerStyle={{width:100, height:30}}
                        textStyle={{fontSize:14}}
                    />
                </View>
            ) : (
                
                <View style={{width:'100%', alignItems:"center"}}>
                <PrimaryButtonLarge
                    title={props.buttonTitle}
                    onPress={props.onPressCardButton}
                    containerStyle={props.containerButton}
                    textStyle={props.buttonText}
                />
            </View>
            )}
            
        </View>
    )
}

export const NoteCard = (props) => {
    return (
        <TouchableOpacity onPress={props.onPressNote} style={{...styles.noteCardContainer, backgroundColor:props.notesColor, ...props.containerStyle}}>
            <View style={styles.noteCardContent}>
                <Text style={styles.noteCardTitle}>{props.title}</Text>
                    {props.pinState === true 
                    ?   <TouchableWithoutFeedback onPress={props.onPressPin}>
                            <Image source={require('../Assets/Icons/PinnedNotes_light.png')} style={{
                            width: 16, 
                            height:16,
                            alignSelf:'center',
                            tintColor: '#342D50'
                            }}/>
                        </TouchableWithoutFeedback>
                     :  <TouchableWithoutFeedback onPress={props.onPressUnpin}>
                            <Image source={require('../Assets/Icons/PinnedNotes_light.png')} style={{
                            width: 16, 
                            height:16,
                            alignSelf:'center',
                            }}/>
                        </TouchableWithoutFeedback>
                    }
            </View>
            <Text style={styles.noteCardTime}>
                {moment.utc(props.time).format('Y MMM D - HH:mm') }
            </Text>
            <Text style={styles.noteCardBody}
                numberOfLines={5}
                >
                {props.body}
            </Text>
        </TouchableOpacity>
    )
}

export const GoalsCard = (props) => {
    return (
        <View style={{...styles.cardContainer, }}>
            <View style={{flexDirection:"row", width: 150, justifyContent:'space-around'}}>
                <Image source={require('../Assets/Icons/AddProgressIcon.png')} style={{width:24, height:24}}/>
                <Text style={{fontFamily:"Poppins-Regular"}}>Add Progress</Text>
            </View>

            <View>
                <Text style ={{fontFamily:'Poppins-Regular', padding:10, fontSize:16}}>Progress</Text>
                <View style={{width:'100%', flexDirection:'row'}}>
                    <SmallInputField 
                        onChangeText={props.onChangeValue}
                        value={props.inputValue}
                        keyboardType="numeric"
                        onSubmitEditing={props.onSubmitEditing}
                        maxLength={props.maxLength}
                    />
                    <SmallInputField 
                        value={props.typeValue}
                        caretHidden={true}
                        editable={false}
                    />
                </View>
            </View>
            <PrimaryButtonMed 
                title="Save"
                containerStyle={{
                    width: 200,
                    height: 40
                }}
                textStyle={{
                    fontSize:16
                }}
                onPress={props.onPressSave}
            />
        
        </View>
    )
}

export const PasswordCard = (props) => {
    const [newPasswordPlaceholder, setNewPasswordPlaceholder] = useState('New password')
    const [confirmPasswordPlaceholder, setConfirmPasswordPlaceholder] = useState('Confirm Password')
    const [emailPlaceholder, setEmailPlaceholder] = useState('Your Email')
    const [oldPasswordPlaceholder, setOldPasswordPlaceholder] = useState('Old password')

    return(
        <View style={{...styles.cardContainer, ...props.cardStyle}}>
            <Text style={{color:globalStyle.TCdark, fontFamily:'Poppins-Regular'}}>
                {props.emailState ? "Forgot Password" : "Change Password"}
            </Text>
            
            {props.emailState 
            ?
                <InputField 
                    title={emailPlaceholder}
                    onChangeText={props.onChangeEmail}
                    onFocus={() => setEmailPlaceholder('')}
                    onBlur={() => setEmailPlaceholder('Your Email')}
                    value={props.valueEmail}
                />
            :   <View style={{width:'100%', height:150,justifyContent:'space-between', marginBottom:20}}>
                    <PasswordField 
                        title={oldPasswordPlaceholder}
                        onChangeText={props.onChangeOldPassword}
                        onFocus={() => setOldPasswordPlaceholder('')}
                        onBlur={() => setOldPasswordPlaceholder('Old password')}
                        value={props.valueOldPassword}
                        onSubmitEditing={props.onSubmitEditingOld}
                    />
                    {props.validator}
                    <PasswordField 
                        refPassword={props.refPasswordNew}
                        title={newPasswordPlaceholder}
                        onChangeText={props.onChangeNewPassword}
                        onFocus={() => setNewPasswordPlaceholder('')}
                        onBlur={() => setNewPasswordPlaceholder('New password')}
                        value={props.valueNewPassword}
                        onSubmitEditing={props.onSubmitEditingNew}
                    />
                    {props.validatorConfirm}
                    <PasswordField 
                        title={confirmPasswordPlaceholder}
                        refPassword={props.refPassword}
                        onChangeText={props.onChangeConfirmPassword}
                        onFocus={() => setConfirmPasswordPlaceholder('')}
                        onBlur={() => setConfirmPasswordPlaceholder('Confirm password')}
                        value={props.valueConfirmPassword}
                        onSubmitEditing={props.onSubmitEditingConfirm}
                    />
                </View>
            }
            <PrimaryButtonLarge 
                title={props.emailState ? 'Send' : 'Save'}
                onPress={props.onPressSave}
            />
        </View>
    )
}


export const LoadingCard = (props) => {
    return(
        <View style={styles.cardContainer}>
            <ActivityIndicator size='large' color='blue'/>
        </View>
    )
}

const styles = StyleSheet.create({
    cardContainer : {
        width:'100%',
        height: '100%',
        backgroundColor:'#F1F4FA',
        alignItems:"center",
        shadowColor:'black',
        shadowRadius:100,
        shadowOpacity:1,
        elevation:8,
        justifyContent:'space-evenly',
        paddingHorizontal:20,
        borderRadius:20
    },
    noteCardContainer: {
        // flexDirection  : 'row',
        // justifyContent : 'flex-start',
        // alignItems     : 'flex-start',
        // flexWrap       : 'wrap',
        flex:1/2,
        height:200,
        margin:5,
        padding:10, 
        borderRadius:20, 
        alignSelf:'center',
    },
    noteCardContent :{
        width:'100%', 
        flexDirection:'row', 
        justifyContent:'space-between'
    },
    noteCardTitle : {
        fontFamily:'Poppins-Medium', 
        fontSize:10, 
        color:'#342D50'
    },
    noteCardTime : {
        fontFamily:'Poppins-regular', 
        fontSize:8, 
        color:'#342D50'
    },
    noteCardBody : {
        fontFamily:'Poppins-regular', 
        fontSize:8, 
        color:'#342D50',
    },


})
