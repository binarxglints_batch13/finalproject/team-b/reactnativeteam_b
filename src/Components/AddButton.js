import React, { useRef, useState } from 'react'
import { StyleSheet, Text, TouchableHighlight, View, Animated, TouchableWithoutFeedback, TouchableOpacity, Touchable } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { useDispatch, useSelector } from 'react-redux'


const AddButton = (props) => {
    const dispatch = useDispatch()
    const buttonState = useSelector(state => state.modal.buttonState)
    const mode = useRef(new Animated.Value(0)).current
    const handleAddButton = () => {
        dispatch({type: 'OPEN_MODAL'})
    }

    if(buttonState) {
        Animated.sequence([
            Animated.timing(mode, {
                toValue: 1,
                timing: 1000,
                useNativeDriver: true
            })
        ]).start();
    } else {
        Animated.sequence([
            Animated.timing(mode, {
                toValue: 0,
                timing: 1000,
                useNativeDriver: true
            })
        ]).start();
    }

    return (
        // <View style={styles.container}>
        //         <TouchableOpacity onPress={() => handleAddButton()} style={styles.button}>
        //             <Animated.View style={{transform: [{rotate: mode.interpolate({
        //                 inputRange:[0,1],
        //                 outputRange: ["0deg", "45deg"]
        //             })}]}}>
        //                 <Icon name='plus' size={30} color='#B6C6E5'/>
        //             </Animated.View>
        //         </TouchableOpacity>
        // </View>
        <TouchableWithoutFeedback onPress={() => handleAddButton()}>
            <Animated.Image source={require('../Assets/Icons/Add.png')} style={{
                position: 'absolute',
                bottom: 0,
                width:50, 
                height:50,
                shadowColor:"black",
                shadowRadius: 50,
                tintColor: !buttonState? "#F1F4FA" : "#625BAD",
                transform: [
                    {rotate: mode.interpolate({
                        inputRange: [0 , 1],
                        outputRange: ["0deg", "45deg"]
                    })}
                ],
                ...props.buttonStyle
                }} />
        </TouchableWithoutFeedback>
    )
}

export default AddButton

const styles = StyleSheet.create({
    container : {
        position:'absolute', 
        top:0,
    },
    button : {
        backgroundColor: '#FFFFFF',
        alignItems:'center',
        justifyContent:'center',
        width: 50,
        height: 50,
        borderRadius:50,
        shadowColor:'black',
        shadowRadius: 5,
        shadowOpacity: 0.5,
        elevation: 5,
    }
})
