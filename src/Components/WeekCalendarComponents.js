import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import { Calendar, LocaleConfig, WeekCalendar } from 'react-native-calendars'
import { todayMonthName, todayYear } from '../Util/DateFormat'
import moment from 'moment'
import globalStyle from '../Util/globalStyle'
import { useDispatch } from 'react-redux'

LocaleConfig.locales['en'] = {
    monthNames: ["January", "Februrary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    dayNames: ['Sunday, Monday, Tuesday, Wednesday', 'Thursday', 'Friday', 'Saturday'],
    dayNamesShort: ['S','M','T','W','T','F','S'],
    today: 'Today'
};
LocaleConfig.defaultLocale = 'en';


const WeekCalendarComponents = (props) => {
    const todayDate = moment().format('YYYY-MM-DD');
    const dispatch = useDispatch()
    const [dayState, setDayState] = useState(todayDate)

    useEffect(() => {
        dispatch({type:'GET_NOTES_BY_DATE', date: dayState})
    }, [dayState])

    return (
        <View style={{width:"100%"}}>
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                <Text style={{fontFamily:'Poppins-Regular', fontSize:14, marginHorizontal:20, color: globalStyle.TCdark}}>{todayMonthName}, {todayYear}</Text>
                <TouchableOpacity onPress={props.onPressCalendar}>
                    <Text style={{fontFamily:'Poppins-Regular', fontSize:14, marginHorizontal:20, textDecorationLine:'underline', color:globalStyle.TCdark}}>See Calendar</Text>
                </TouchableOpacity>
            </View>
            <WeekCalendar
                markingType={'multi-dot'}
                markedDates={props.markedDates}
                theme={{
                    backgroundColor:"#F1F4FA",
                    calendarBackground:'#F1F4FA',
                    textSectionTitleColor:'#342D50',
                    textDayFontFamily: 'Poppins-Regular',
                    textDayHeaderFontFamily: 'Poppins-Regular',
                    disabledArrowColor:'#B6C6E5',
                    textDayFontSize:14,
                    textDayHeaderFontSize:12,
                    'stylesheet.calendar.main' : {
                        week: {
                            flexDirection: 'row',
                            justifyContent: 'space-around'
                          },
                    },
                    
                }}
                renderArrow={(direction) => {
                    direction === "left" 
                    ? (<MaterialIcon name='keyboard-arrow-left' size={20}/>)
                    : (<Image source={require('../Assets/Icons/BackArrow.png')} style={{width:10, height:16}}/>)
                }}
                dayComponent={(props) => {
                    
                    return (
                    <View>
                        <TouchableOpacity onPress={() => {
                                setDayState(props.date.dateString)
                                
                            }} >
                            <View style={[{alignItems:'center',width:30, height:30}, dayState === props.date.dateString ? styles.selectedDate : null]}>
                                <Text style={{...styles.customDay, color : dayState === props.date.dateString ? "#F1F4FA" : "#342D50"}}>
                                {props.date.day}
                                </Text>
                                {props.marking 
                                ? props.marking.dots.length > 1 ? (
                                <View style={{flexDirection:'row', }}>
                                    <View style={{width:5,height:5,borderRadius:20, marginHorizontal:1 ,backgroundColor:props.marking.dots[0].color}}/> 
                                    <View style={{width:5,height:5,borderRadius:20, backgroundColor:props.marking.dots[1].color}}/>
                                </View> ) : (
                                <View style={{flexDirection:'row'}}>
                                    <View style={{width:5,height:5,borderRadius:20,backgroundColor:props.marking.dots[0].color}}/> 
                                </View> ) : null}
                            </View>
                        </TouchableOpacity>
                    </View>
                    );
                }}
                />
        </View>
    )
}

export default WeekCalendarComponents

const styles = StyleSheet.create({
    customDay: {
        textAlign: 'center',
        fontFamily:'Poppins-Regular'
      },
    disabledText: {
        color: 'grey'
    },
    defaultText: {
        color: 'black'
      },
    selectedDate: { 
        borderRadius:40,
        backgroundColor:'#625BAD'
    }
})
