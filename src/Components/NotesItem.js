import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import globalStyle from '../Util/globalStyle'

const NotesItem = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress} style={{
            width:225, 
            height:120,
            margin:10, 
            marginRight:20,
            borderRadius:20,
            padding:10,
            elevation: 4,
            backgroundColor:props.noteColor
            }}>
            <View style={{flexDirection:'row', justifyContent:"space-between"}}>
                <Text 
                style={{fontFamily:'Poppins-Regular', 
                fontSize:14, color: globalStyle.TCdark}} 
                numberOfLines={1}>{props.title}</Text>
                <Image source={require('../Assets/Icons/PinnedNotes_light.png')} style={{
                width: 16, 
                height:16,
                tintColor: '#342D50'
                }}/>
            </View>
            <Text style={{fontFamily:'Poppins-Light', fontSize:12, color: globalStyle.TCdark}}
                numberOfLines={4}    
                >
                {props.bodyNote}</Text>
        </TouchableOpacity>
    )
}

export default NotesItem

const styles = StyleSheet.create({})
