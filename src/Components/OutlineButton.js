import React from 'react'
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import globalStyle from '../Util/globalStyle'


// ======================== Outline Button Large =============================
export const OutlineButtonLarge = (props) => {
    return (
        <TouchableOpacity 
        style={{...styles.container, ...props.containerStyle}}
        onPress={() => props.onPress()}>
            <Text style={{...styles.buttonText,...props.textStyle}}>{props.title}</Text>
        </TouchableOpacity>
    )
}


// ======================== Outline Button Med =============================
export const OutlineButtonMed = (props) => {
    return(
        <TouchableOpacity 
            style={{...styles.containerMed, ...props.containerStyle}}
            onPress={() => props.onPress()}>
                <Text style={{...styles.buttonText, ...props.textStyle}}>{props.title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container : {
        width: "100%",
        height: 50,
        borderRadius: 100,
        alignItems:'center',
        justifyContent:'center',
        marginVertical:10,
        borderWidth:1,
        borderColor: globalStyle.secondaryColorDark
    },
    containerMed : {
        width: "50%",
        height: 50,
        borderRadius: 100,
        alignItems:'center',
        justifyContent:'center',
        borderWidth:1,
        borderColor: globalStyle.secondaryColorDark
    },
    buttonText : {
        color: globalStyle.secondaryColorDark, 
        fontFamily: "Poppins-SemiBold", 
        fontSize:20
    }
})
