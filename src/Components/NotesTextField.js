import React from 'react'
import { Keyboard, StyleSheet, Text, TextInput, TouchableWithoutFeedback, View } from 'react-native'


// ======================== Notes Input Title =============================
export const NotesInputTitle = (props) => {
    return (
        <View style={{...styles.container, ...props.style}}>
            <TextInput 
                placeholder={props.title}
                onFocus={() => props.onFocus()}
                onBlur={() => props.onBlur()}
                onChangeText={props.onChangeText}
                value={props.value}
                maxLength={25}
                style={styles.field}
                editable={props.editable}
           />
        </View>
    )
}


// ======================== Notes Input Body =============================
export const NotesInputBody = (props) => {
    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={{
            ...styles.container,
             borderBottomWidth:0,
             height:'90%',
             }}>

            <TextInput 
            placeholder={props.title}
            onFocus={() => props.onFocus()}
            onBlur={() => props.onBlur()}
            onChangeText={props.onChangeText}
            onSubmitEditing={props.onSubmitEditing}
            keyboardType="default"
            returnKeyType="done"
            onKeyPress={props.onKeyPress}
            multiline={true}
            maxLength={props.maxLength}
            blurOnSubmit={props.blurOnSubmit}
            value={props.value}
            textAlign="center"
            textAlignVertical="top"
            editable={props.editable}
            style={{...styles.field, fontFamily:'Poppins-thin', fontSize: 18, ...props.style}}
           />
        </View>
        </TouchableWithoutFeedback>
    )
}


const styles = StyleSheet.create({
    container : {
        width:'100%', 
        alignItems:'center', 
    },
    field : {
        width:"100%",  
        paddingVertical:0, 
        color:'#342D50',
        fontFamily: "Poppins-Medium",
        fontSize: 20,
        textAlign:'justify'
    }
})
