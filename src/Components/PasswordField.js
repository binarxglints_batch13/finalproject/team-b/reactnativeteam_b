import React, { useState } from 'react'
import { StyleSheet, Image, TextInput, TouchableWithoutFeedback, View, Keyboard } from 'react-native'
import Icon from 'react-native-vector-icons/Octicons'
import globalStyle from '../Util/globalStyle'

const PasswordField = (props) => {
    const [eyeState, setEyeState] = useState(true)

    return (
        <View style={{...styles.container, ...props.textContainerStyle}}>
            <TextInput
            ref={props.refPassword}
            placeholder={props.title}
            onFocus={props.onFocus}
            onBlur={props.onBlur}
            onChangeText={props.onChangeText}
            value={props.value}
            maxLength={20}
            secureTextEntry={eyeState ? true : false}
            style={{...styles.field, ...props.textStyle}}
            blurOnSubmit={false}
            onSubmitEditing={props.onSubmitEditing}
           />
           <TouchableWithoutFeedback onPress={() => setEyeState(!eyeState)} style={{position:'absolute', top:20}}>
               {eyeState ? <Icon name="eye-closed" size={20} color={globalStyle.secondaryColorDark} /> 
               : <Icon name="eye" size={20} color={globalStyle.secondaryColorDark} />}
           </TouchableWithoutFeedback>
        </View>
    )
}

export default PasswordField

const styles = StyleSheet.create({
    container : {
        width:'100%', 
        height:40, 
        borderWidth:1, 
        borderRadius: 10,
        borderColor:'#B6C6E5',
        alignItems:'center', 
        flexDirection: 'row',
        paddingHorizontal:20,
    },
    field : {
        width:"80%",  
        paddingVertical:0, 
        color:'#342D50',
        fontFamily: "Poppins-Regular"
    }
})
