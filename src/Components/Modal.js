import React, { useEffect, useRef } from 'react'
import { Modal, StyleSheet, Text, TouchableWithoutFeedback, View, Animated, TextInput, Image, TouchableOpacity, ActivityIndicator, KeyboardAvoidingView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { BlurView } from '@react-native-community/blur'

import RoundButton from './RoundButton'
import MonthlyCalendarComponent from './MonthlyCalendarComponent'
import { PrimaryButtonLarge } from './PrimaryButton'
import { SecondaryButtonLarge } from './SecondaryButton'
import { AlertCard, GoalsCard, LoadingCard, PasswordCard, } from './Card'
import globalStyle from '../Util/globalStyle'


// ======================== Home Modal =============================
export const HomeModal = (props) => {
    const dispatch = useDispatch()

    return (
        <Modal
            animationType="fade"
            visible={props.modalVisible}
            transparent={true}
            onRequestClose={() => {
                dispatch({type: 'CLOSE_MODAL'})
            }}
             >

            <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

                <TouchableWithoutFeedback
                onPress={() => {
                    dispatch({type: 'CLOSE_MODAL'})
                }}
                >
                    <View style={styles.modalOverlay}/>
                </TouchableWithoutFeedback>
                
                <View style={{position:'absolute', bottom:'12%', left:'30%'}}>
                    <RoundButton 
                    image={require('../Assets/Icons/fluent_target-arrow-16-filled.png')} backgroundColor="#FF586A"
                    title="Goals"
                    onPress={() => props.toGoals()}
                    />
                </View>
                <View style={{position:'absolute', bottom:'12%', left:'55%'}}>
                    <RoundButton image={require('../Assets/Icons/clarity_note-edit-solid.png')} backgroundColor="#41CFC1"
                    title="Notes"
                    onPress={() => props.toNotes()}
                    />
                </View>
        </Modal>
    )
}


// ======================== Calendar Modal =============================
export const CalendarModal = (props) => {
    const dispatch = useDispatch()
    const modalCalendarState = useSelector(state => state.modal.modalCalendarState)
    
    const transition = useRef(new Animated.Value(0)).current

    useEffect(() => {
        if(modalCalendarState) {
            Animated.sequence([
                Animated.timing(transition, {
                    toValue: 0,
                    timing: 1000,
                    useNativeDriver: true
                })
            ]).start();
        } else {
            Animated.sequence([
                Animated.timing(transition, {
                    toValue: 1,
                    timing: 1000,
                    useNativeDriver: true
                })
            ]).start();
        }
    }, [modalCalendarState])
    


    return (
        <KeyboardAvoidingView behavior='position' style={{flex:1}}>
        <Modal
            animationType="fade"
            visible={props.modalVisible}
            transparent={true}
            onRequestClose={() => {
                dispatch({type: 'CLOSE_MODAL_CALENDAR'})
            }}
        >
            <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

                <TouchableWithoutFeedback
                    onPress={() => {
                        dispatch({type: 'CLOSE_MODAL_CALENDAR'})
                    }}
                >
                    <View style={styles.modalOverlay}/>
                </TouchableWithoutFeedback>
                
                <Animated.View 
                style={{
                    ...styles.cardCalendar,
                    transform : [{
                        translateY: transition.interpolate({
                            inputRange: [0,1],
                            outputRange:[0,450]
                        })
                    }]}}>
                        <View style={{width:'100%', height:"100%", alignItems:"center",}}>
                            <View style={{position:'absolute', right:"10%", top:'5%', zIndex:1}}>
                                <TouchableOpacity onPress={() => dispatch({type: 'CLOSE_MODAL_CALENDAR'})}>
                                    <Image source={require('../Assets/Icons/CrossIcon.png')} style={{width:20, height:20, tintColor:'#F1F4FA'}}/>
                                </TouchableOpacity>
                            </View>
                            <View style={{marginTop:20, width:"100%"}}>
                                <MonthlyCalendarComponent
                                    onDayPress={props.onDayPress}
                                    hideArrows={props.hideArrows}
                                    enableSwipeMonths={props.enableSwipeMonths}
                                    disableArrowLeft={props.disableArrowLeft}
                                    disableArrowRight={props.disableArrowRight}
                                    markingType={props.markingType}
                                    markedDates={props.markedDates}
                                    theme={{
                                        backgroundColor:"#B6C6E5",
                                        calendarBackground:'#B6C6E5',
                                        textSectionTitleColor:'#342D50',
                                        textDayFontFamily: 'Poppins-Regular',
                                        textDayHeaderFontFamily: 'Poppins-Regular',
                                        disabledArrowColor:'#B6C6E5',
                                        textDayFontSize:12,
                                        textDayHeaderFontSize:12,
                                        textDisabledColor: 'rgba(52, 45, 80, 0.5)',
                                        'stylesheet.calendar.header' : {
                                            monthText : {
                                                fontSize: 13,
                                                width:'100%',
                                                alignSelf: 'center',
                                                paddingHorizontal:5,
                                                fontFamily:'Poppins-Regular',
                                                color:'#342D50'
                                            },
                                        },
                                        'stylesheet.calendar.main' : {
                                            week: {
                                                flexDirection: 'row',
                                                justifyContent: 'space-around'
                                              },
                                        }
                                    }}
                                />
                            </View>
                            
                            <View style={{width:'100%', paddingHorizontal:15, marginVertical:10}}>
                                <Text style={{
                                    fontFamily:'Poppins-Regular',
                                    marginBottom:5
                                }}>Set Time</Text>
                                <View style={{
                                            flexDirection:'row',
                                            alignItems:'center',
                                            justifyContent:'center',
                                            borderWidth:1,
                                            width:80,
                                            height:30
                                        }}>
                                    <TextInput 
                                        style={{margin:0, padding:0}}
                                        keyboardType='number-pad'
                                        placeholder="hh"
                                        onChangeText={props.onChangeHour}
                                        value={props.valueHour}
                                        maxLength={props.maxLengthHour}
                                        onSubmitEditing={props.onSubmitEditingHour}
                                        blurOnSubmit={false}
                                    />
                                    <Text >{":"}</Text>
                                    <TextInput 
                                        style={{margin:0, paddingVertical:0}}
                                        ref={props.refMin}
                                        keyboardType='number-pad'
                                        placeholder="mm"
                                        onChangeText={props.onChangeMin}
                                        value={props.valueMin}
                                        maxLength={props.maxLengthMin}
                                        onSubmitEditing={props.onSubmitEditingMin}
                                    />
                                </View>
                                
                            </View>
                    
                            <View style={{width:'100%', paddingHorizontal:10, marginVertical:30}}>
                                <PrimaryButtonLarge 
                                title="Save"
                                
                                onPress={props.onPress}
                                />
                            </View>
                        </View>
                </Animated.View>
        </Modal>
        </KeyboardAvoidingView>
    )
}


// ======================== Alert Modal =============================
export const AlertModal = (props) => {
    return(
        <Modal 
            animationType="fade"
            visible={props.alertModalVisible}
            transparent={true}
        >
            <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

                <TouchableWithoutFeedback
                    onPress={
                        props.onBackdropClick
                    }
                >
                    <View style={styles.modalOverlay}/>
                </TouchableWithoutFeedback>
                
                <View style={{...styles.alertCard, ...props.cardPosition}}>
                    {props.loading 
                    ? <LoadingCard />
                    : <AlertCard 
                        cardTitle={props.cardTitle}
                        cardTitleText={props.cardTitleText}
                        cardImage={props.cardImage}
                        cardText={props.cardText}
                        cardStyle={props.cardStyle}
                        buttonTitle={props.buttonTitle}
                        onPressCardButton={props.onPressCardButton}
                        containerButton={props.containerButton}
                        buttonText={props.buttonText}
                        cardTypeAsk={props.cardTypeAsk}
                        onPressButtonYes={props.onPressButtonYes}
                        onPressButtonNo={props.onPressButtonNo}
                    />
                    }
                    
                </View>

        </Modal>
    )
}

// ======================== Add Progress Modal =============================
export const AddProgressModal = (props) => {

    return (
        <Modal
            animationType="fade"
            visible={props.modalVisible}
            transparent={true}
            onRequestClose={props.onRequestClose}
        >
            <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

            <TouchableWithoutFeedback
                    onPress={props.onBackdropClick}
                >
                    <View style={styles.modalOverlay}/>
            </TouchableWithoutFeedback>

            <View style={{...styles.alertCard, left:'10%', right:'10%', height:250}}>

                <GoalsCard
                    onChangeValue={props.onChangeValue}
                    inputValue={props.inputValue}
                    typeValue={props.typeValue}
                    onPressSave={props.onPressSave}
                    />

            </View>

        </Modal>
    )
}

// ======================== Change Photo Modal =============================
export const ChangePhotoModal = (props) => {
    const transition = useRef(new Animated.Value(0)).current

    useEffect(() => {
        if(props.modalVisible) {
            Animated.sequence([
                Animated.timing(transition, {
                    toValue: 0,
                    timing: 1000,
                    useNativeDriver: true
                })
            ]).start();
        } else {
            Animated.sequence([
                Animated.timing(transition, {
                    toValue: 1,
                    timing: 1000,
                    useNativeDriver: true
                })
            ]).start();
        }
    }, [props.modalVisible])

    return (
        <Modal
            animationType="fade"
            visible={props.modalVisible}
            transparent={true}
            onRequestClose={props.onRequestClose}
            >
            <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

            <TouchableWithoutFeedback
                    onPress={props.onBackdropClick}
                >
                    <View style={styles.modalOverlay}/>
            </TouchableWithoutFeedback>
            
            <Animated.View style={{
                        width:'100%',
                        padding:10, 
                        backgroundColor:globalStyle.primaryColor,
                        flexDirection:'row',
                        justifyContent:'space-between',
                        borderTopWidth:1,
                        borderTopColor:globalStyle.secondaryColorDark,
                        transform : [{
                        translateY: transition.interpolate({
                            inputRange: [0,1],
                            outputRange:[0,450]
                        })
                    }]}}>
                    <TouchableOpacity onPress={props.onPressRemove} style={{alignItems:'center', padding:10}} >
                        <View style={styles.roundIcon}>
                            <Image source={require('../Assets/Icons/RoundRemovePhoto.png')} style={{width:25, height:25}}/>
                        </View>
                        <Text style={{fontFamily:'Poppins-Regular', fontSize:12}}>Remove Photo</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={props.onPressTake} style={{alignItems:'center', padding:10}}>
                        <View style={styles.roundIcon}>
                            <Image source={require('../Assets/Icons/RoundTakePicture.png')} style={{width:20, height:20}}/>
                        </View>
                        <Text style={{fontFamily:'Poppins-Regular', fontSize:12}}>Take Photo</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={props.onPressAdd} style={{alignItems:'center', padding:10}}>
                        <View style={styles.roundIcon}>
                            <Image source={require('../Assets/Icons/RoundAddPhoto.png')} style={{width:20, height:20}}/>
                        </View>
                        <Text style={{fontFamily:'Poppins-Regular', fontSize:12}}>Add Photo</Text>
                    </TouchableOpacity>

            </Animated.View>

        </Modal>
    )
}

// ======================== Password Modal =============================
export const PasswordModal = (props) => {
    return(
        <Modal
            animationType="fade"
            visible={props.modalVisible}
            transparent={true}
            onRequestClose={props.onRequestClose}
        >
            <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

            <TouchableWithoutFeedback
                    onPress={props.onBackdropClick}
                >
                    <View style={styles.modalOverlay}/>
            </TouchableWithoutFeedback>

            <View style={{...styles.alertCard, left:'10%', right:'10%', height:350,}}>

                <PasswordCard 
                    emailState={props.emailState}
                    onChangeEmail={props.onChangeEmail}
                    valueEmail={props.valueEmail}

                    validator={props.validator}
                    validatorConfirm={props.validatorConfirm}

                    onChangeOldPassword={props.onChangeoldPassword}
                    valueOldPassword={props.valueOldPassword}
                    onSubmitEditingOld={props.onSubmitEditingOld}

                    onChangeNewPassword={props.onChangeNewPassword}
                    valueNewPassword={props.valueNewPassword}
                    onSubmitEditingNew={props.onSubmitEditingNew}
                    refPasswordNew={props.refPasswordNew}

                    onChangeConfirmPassword={props.onChangeConfirmPassword}
                    valueConfirmPassword={props.valueConfirmPassword}
                    onSubmitEditingConfirm={props.onSubmitEditingConfirm}
                    refPassword={props.refConfirm}

                    onPressSave={props.onPressSave}
                />

            </View>

        </Modal>
    )
}

// ======================== Password Modal =============================
export const EmailModal = (props) => {
    return (
        <Modal
            animationType="fade"
            visible={props.modalVisible}
            transparent={true}
            onRequestClose={props.onRequestClose}
        >
            <BlurView blurType="light" blurAmount={5} style={{flex:1}}/>

            <TouchableWithoutFeedback
                    onPress={props.onBackdropClick}
                >
                    <View style={styles.modalOverlay}/>
            </TouchableWithoutFeedback>

            <View style={{...styles.alertCard, left:'10%', right:'10%', height:200}}>

                
                {props.loading 
                ? <ActivityIndicator size='large' color='blue' />
                : <PasswordCard 
                    emailState={true}
                    onPressSave={props.onPressSend}
                    onChangeEmail={props.onChangeEmail}
                    valueEmail={props.valueEmail}
                />
                }

            </View>

        </Modal>
    )
}

const styles = StyleSheet.create({
    modalOverlay : {
        flex:1,
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
        backgroundColor:'transparent'
    },
    cardCalendar : {
        width:'100%', 
        height: 450,
        alignItems:"center",
        backgroundColor:'#B6C6E5',
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        padding:10
    },
    alertCard : {
        position:'absolute',
        left:'15%',
        right:'15%',
        top:'25%',
        height:'40%',
    },
    roundIcon : { 
        width:50, 
        height:50 ,
        backgroundColor:globalStyle.secondaryColorDark,
        alignItems:'center', 
        justifyContent:'center',
        borderRadius:100
    }
})
