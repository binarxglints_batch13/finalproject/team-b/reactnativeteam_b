import React, { useState } from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'

const ColorPicker = (props) => {

    return (
        <View>
            <TouchableOpacity 
                onPress={() => props.onPress()}
                style={{...styles.buttonContainer, backgroundColor: props.colors}}
            >
                { props.pickerState 
                ?   <View style={styles.checkContainer}>
                        <Image source={require('../Assets/Icons/CheckIcon.png')} style={{width:24, height:24,}}/>
                    </View> : null
                }
            </TouchableOpacity>
        </View>
    )
}

export default ColorPicker

const styles = StyleSheet.create({
    buttonContainer : {
        width:50, 
        height:50,  
        borderRadius:50
    },
    checkContainer : {
        width:50, 
        height:50, 
        backgroundColor:'transparent', 
        borderWidth:1, 
        alignItems:'center', 
        justifyContent:"center", 
        borderRadius:50
    }
})
