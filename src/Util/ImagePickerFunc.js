import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import { useDispatch, useSelector } from 'react-redux'

// const dispatch = useDispatch()

export const launchImageLib = async (props) => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    console.log("test",props)
    let launch = await launchImageLibrary({mediaType:'photo', includeBase64:true}, async (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let tempRes = response
        const source = tempRes.assets[0]
        props(source)
        console.log("tempres", source)
        console.log('response', JSON.stringify(response));
        return source
      }
    });
    console.log('launch', launch)

} 

export const launchCameraLib = async (props) => {
  let options = {
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  console.log("test",props)
  let launch = await launchCamera({mediaType:'photo'}, async (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
      alert(response.customButton);
    } else {
      let tempRes = response
      const source = tempRes.assets[0]
      props(source)
      console.log("tempres", source)
      console.log('response', JSON.stringify(response));
      return source
    }
  });
  console.log('launch', launch)

} 