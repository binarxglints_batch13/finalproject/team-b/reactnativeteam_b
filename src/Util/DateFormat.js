const monthName = ["January", "Februrary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

const date = new Date()

export const todayDay = date.getDate()
export const todayMonth = date.getMonth()
export const todayYear = date.getFullYear()
export const todayMonthName = monthName[todayMonth]
export const todayDate = `${todayYear}-${todayMonth}-${todayDay}`