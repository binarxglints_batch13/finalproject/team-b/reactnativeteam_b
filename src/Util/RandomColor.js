


export function getRandomColor() {
    const colors = ['#FFBCC2', '#FCF3A1', '#CCF0D7', '#FF8888', '#D1CDFA']
    return colors[Math.floor(Math.random() * colors.length)]
}

