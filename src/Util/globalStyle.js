const globalStyle = {
    primaryColor : '#F1F4FA',
    secondaryColorLight : '#B6C6E5',
    secondaryColorDark : '#625BAD',
    CCpink : '#FFBCC2',
    CClemon: '#FCF3A1',
    CCgreen: '#CCF0D7',
    CCred : '#FF8888',
    CCpurple: '#D1CDFA',
    TClight: '#F1F4FA',
    TCdark : '#342D50',
    strokeColorPink :'#FF8888',
    strokeColorLemon :'#F6E64C',
    strokeColorGreen :'#34A69A',
    strokeColorRed :'#FF586A',
    strokeColorPurple :'#A258FF',
}

export default globalStyle;