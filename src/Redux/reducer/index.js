import { combineReducers } from "redux";
import modal from "./modal";
import colors from "./colorReducer";
import notes from "./notesReducer";
import register from './RegisterReducer';
import auth from "./authReducer";
import goals from "./GoalsReducer";
import user from "./userReducer";

export default combineReducers({
    modal,
    colors,
    notes,
    register,
    auth,
    goals,
    user
})