const initialState = {
    notesData : [],
    notesDataDetail: [],
    notesDatabyDate: [],
    isLoadingDataDate: false,
    isLoadingData : false,
    isLoadingPost : false,
    isLoadingDelete : false,
    modalStatePost : false,
    modalStateData: false,
    modalStateDelete: false,
    statusCode : null,
    notesEditState: false,
    deleteAskState: false,
}

const notes = (state = initialState, action) => {
    switch(action.type) {
        case 'EDIT_STATE' :
            return {
                ...state,
                notesEditState: true
            }
        case 'READ_STATE' :
            return {
                ...state,
                notesEditState: false
            }
        case 'POST_NOTES':
            return {
                ...state,
                modalStatePost : true,
                isLoadingPost : true,
            }
        case 'POST_NOTES_SUCCESS':
            return {
                ...state,
                statusCode : action.message,
                isLoadingPost : false,
                modalStatePost : true,
            }
        case 'POST_NOTES_FAILED':
            return {
                ...state,
                statusCode : action.message,
                isLoadingPost : false,
                modalStatePost : true,
            }
        case 'POST_SUCCESS_OK':
            return {
                ...state,
                statusCode : null,
                modalStatePost: false,
            }
        case 'POST_FAILED_OK':
            return {
                ...state,
                statusCode: null,
                modalStatePost: false,
            }
        case 'GET_NOTES_DATA':
            return {
                ...state,
                isLoadingData: true,
            }
        case 'GET_NOTES_DATA_SUCCESS':
            return {
                ...state,
                notesData: action.notesData,
                isLoadingData: false,
            }
        case 'GET_NOTES_DATA_FAILED':
            return {
                ...state,
                isLoadingData: false,
            }
        case 'GET_NOTES_DETAIL':
            return {
                ...state,
                isLoadingData: true,
            }
        case 'GET_NOTES_DETAIL_SUCCESS':
            return {
                ...state,
                notesDataDetail: action.notesDetail,
                isLoadingData: false,
            }
        case 'GET_NOTES_DETAIL_FAILED':
            return {
                ...state,
                isLoadingData: false,
            }
        case 'DELETE_MODAL_OPEN' :
            return {
                ...state,
                modalStateDelete: true,
                deleteAskState: true,
            }
        case 'DELETE_MODAL_CLOSE' :
            return {
                ...state,
                modalStateDelete: false,
                deleteAskState: false,
            }
        case 'DELETE_NOTES' :
            return {
                ...state,
                deleteAskState: false,
                isLoadingDelete : true
            }
        case 'DELETE_NOTES_SUCCESS' :
            return {
                ...state,
                statusCode : action.message,
                isLoadingDelete : false
            }
        case 'DELETE_NOTES_FAILED' :
            return {
                ...state,
                statusCode : action.message,
                isLoadingDelete : false
            }
        case 'DELETE_SUCCESS_OK' :
            return {
                ...state,
                statusCode : null,
                modalStateDelete : false
            }
        case 'DELETE_FAILED_OK' :
            return {
                ...state,
                statusCode : null,
                modalStateDelete : false
            }
        case 'EDIT_NOTE':
            return {
                ...state,
                modalStatePost : true,
                isLoadingPost : true,
            }
        case 'EDIT_NOTE_SUCCESS':
            return {
                ...state,
                statusCode : action.message,
                isLoadingPost : false,
                modalStatePost : true,
            }
        case 'EDIT_NOTE_FAILED':
            return {
                ...state,
                statusCode : action.message,
                isLoadingPost : false,
                modalStatePost : true,
            }
        case 'EDIT_SUCCESS_OK':
            return {
                ...state,
                statusCode : null,
                modalStatePost: false,
            }
        case 'EDIT_FAILED_OK':
            return {
                ...state,
                statusCode: null,
                modalStatePost: false,
            }
        case 'GET_NOTES_BY_DATE' :
            return {
                ...state,
                isLoadingDataDate : true,
            }
        case 'GET_NOTES_BY_DATE_SUCCESS':
            return {
                ...state,
                notesDatabyDate : action.notesData,
                isLoadingDataDate : false,
            }
        case 'GET_NOTES_BY_DATE_FAILED':
            return {
                ...state,
                notesDatabyDate : [],
                isLoadingDataDate : false,
            }
        case 'RESET_STATE':
            return {
                notesData : [],
                notesDataDetail: [],
                isLoadingData : false,
                isLoadingPost : false,
                isLoadingDelete : false,
                modalStatePost : false,
                modalStateData: false,
                modalStateDelete: false,
                statusCode : null,
                notesEditState: false,
                deleteAskState: false,
            }
        default :
            return state;
    }
}

export default notes