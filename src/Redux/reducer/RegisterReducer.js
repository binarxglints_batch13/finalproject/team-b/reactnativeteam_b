const initialState = {
    message: null,
    firstName: '',
    lastName:'',
    userName:'',
    email:'',
    password:'',
    isLoading: false,
    statusCode: '',
    modalState: false,
}

const register = (state = initialState, action) => {
    switch(action.type) {
        case 'FIRST_NAME':
            return {
                ...state,
                firstName: action.firstName
            }
        case 'LAST_NAME':
            return {
                ...state,
                lastName: action.lastName
            }
        case 'USERNAME':
            return{
                ...state,
                userName: action.userName
            }
        case 'EMAIL':
            return {
                ...state,
                email: action.email
            }
        case 'PASSWORD' :
            return {
                ...state,
                password: action.password
            }
        case 'REGISTER' :
            return {
                ...state,
                modalState: true,
                isLoading: true
            }
        case 'REGISTER_SUCCESS' :
            return {
                ...state,
                statusCode: action.status,
                isLoading: false
            }
        case 'REGISTER_FAILED' :
            return {
                ...state,
                message : action.messagePayload.message,
                statusCode: action.messagePayload.status,
                isLoading: false
            }
        case 'REGISTER_SUCCESS_OK' :
            return {
                ...state,
                statusCode:'',
                modalState: false,
            }
        case 'REGISTER_FAILED_OK' :
            return {
                ...state,
                message: '',
                statusCode:'',
                modalState: false,
            }
        default:
            return state
    }
}

export default register