const initialState = {
    buttonState : false,
    modalState : false,
    modalCalendarState: false
}

const modal = (state =  initialState, action) => {
    switch(action.type) {
        case 'OPEN_MODAL':
            return {
                ...state,
                buttonState: true,
                modalState: true,
            }
        case 'CLOSE_MODAL':
            return {
                ...state,
                buttonState: false,
                modalState: false,
            }
        case 'OPEN_MODAL_CALENDAR':
            return {
                ...state,
                modalCalendarState: true
            }
        case 'CLOSE_MODAL_CALENDAR':
            return {
                ...state,
                modalCalendarState: false
            }
        default:
            return state
    }
}

export default modal