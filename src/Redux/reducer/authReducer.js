const initialState = {
    loading : false,
    loadingChangePassword: false,
    statusCode : '', 
    modalState : false,
    changeModalState :false,
    forgotModalState : false,
    isLoggedIn : false,
    verifyCheck: null,
    emailStore: '', 
    message: '',
}

const auth = (state = initialState, action) => {
    switch(action.type) {
        case 'LOGIN' :
            return {
                ...state,
                modalState : true,
                loading: true,
            }
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                loading: false,
                statusCode: action.message,
            }
        case 'LOGIN_FAILED':
            return {
                ...state,
                statusCode : action.messagePayload.status,
                verifyCheck: action.messagePayload.verifyCheck,
                emailStore: action.messagePayload.emailStore,
                loading: false,
            }
        case 'FAILED_OK':
            return {
                ...state,
                statusCode : '',
                verifyCheck: null,
                modalState: false
            }
        case 'SUCCESS_OK':
            return {
                ...state,
                isLoggedIn: true,
                modalState: false
            }
        case 'VERIFY_TOKEN':
            return {
                ...state,
                isLoggedIn: true
            }
        case 'LOGOUT':
            return {
                ...state,
                statusCode : '',
                isLoggedIn: false
            }
        case 'SEND_VERIFY' : 
            return {
                ...state,
                emailStore:'',
                verifyCheck: null,
                statusCode: '',
                loading: true,
            }
        case 'SEND_VERIFY_SUCCESS':
            return {
                ...state,
                statusCode: action.messagePayload.status,
                message: action.messagePayload.message,
                loading: false
            }
        case 'SEND_VERIFY_FAILED':
            return {
                ...state,
                loading: false
            }
        case 'SEND_VERIFY_OK' :
            return {
                ...state,
                statusCode : '',
                message : '',
                modalState : false,
            }
        case 'OPEN_FORGOT' :
            return {
                ...state,
                forgotModalState : true
            }
        case 'CLOSE_FORGOT' :
            return {
                ...state,
                forgotModalState : false
            }
        case 'FORGOT_SEND' :
            return {
                ...state,
                loading: true,
            }
        case 'FORGOT_SEND_SUCCESS' :
            return {
                ...state,
                statusCode : action.messagePayload.status,
                message: action.messagePayload.message,
                loading: false,
            }
        case 'FORGOT_SEND_FAILED' :
            return {
                ...state,
                statusCode : action.messagePayload.status,
                message: action.messagePayload.message,
                loading: false,
            }
        case 'FORGOT_OK' :
            return  {
                ...state,
                statusCode : '',
                message :'',
                forgotModalState: false
            }
        case 'FORGOT_OK' :
            return  {
                ...state,
                statusCode : '',
                message :'',
                forgotModalState: false
            }
        case 'CHANGE_PASSWORD_AUTH' :
            return {
                ...state,
                changeModalState: true, 
                loadingChangePassword: true,
            }
        case 'CHANGE_PASSWORD_AUTH_SUCCESS':
            return {
                ...state,
                message: action.message,
                loadingChangePassword : false
            }
        case 'CHANGE_PASSWORD_AUTH_FAILED':
            return {
                ...state,
                message: action.message,
                loadingChangePassword : false
            }
        case 'CHANGE_PASSWORD_AUTH_OK' :
            return {
                ...state,
                changeModalState: false,
            }
        case 'CHANGE_PASSWORD_AUTH_FAILED' : 
            return {
                ...state,
                changeModalState: false,
            }
        default :
            return state
    }
}

export default auth