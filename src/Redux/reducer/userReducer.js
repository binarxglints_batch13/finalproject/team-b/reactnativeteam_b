const initialState = {
    userData : [],
    isLoading : false,
    message: null,
}

const user = (state = initialState, action) => {
    switch(action.type) {
        case 'GET_USER_DATA': 
            return {
                ...state,
                isLoading: true
            }
        case 'GET_USER_DATA_SUCCESS':
            return {
                ...state,
                userData : action.userData,
                isLoading: false
            }
        case 'UPDATE_PROFILE' :
            return {
                ...state,
                isLoading: true,
            }
        case 'UPDATE_PROFILE_SUCCESS' :
            return {
                ...state,
                message: action.message,
                isLoading: false,
            }
        case 'UPDATE_PROFILE_FAILED' :
            return {
                ...state,
                message: action.message,
                isLoading: false,
            }
        case 'RESET_STATE':
            return {
                userData : [],
                isLoading : false,
                message: null,
            }
        case 'CHANGE_PASSWORD':
            return{
                ...state,
                isLoading: true
            }
        case 'CHANGE_PASSWORD_SUCCESS' :
            return {
                ...state,
                message: action.message,
                isLoading: false,
            }
        case 'CHANGE_PASSWORD_FAILED' :
            return {
                ...state,
                message: action.message,
                isLoading: false,
            }
        case 'ALERT_CLOSE_OK':
            return{
                ...state,
                message : null
            }
        default:
            return state
    }
}

export default user