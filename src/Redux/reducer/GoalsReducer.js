const initialState = {
    goalsData : [],
    goalsDataDetail: [],
    isLoadingData: false, 
    isLoadingPost: false,
    isLoadingEdit: false,
    isLoadingDetail: false,
    statusCode: null,
    modalState: false,
    modalStateEdit: false,
    modalStateDetail: false,
}

const goals = (state = initialState, action) => {
    switch(action.type) {
        case 'GET_DATA_GOALS' :
            return {
                ...state,
                isLoadingData: true
            }
        case 'GET_DATA_GOALS_SUCCESS':
            return {
                ...state,
                goalsData: action.dataResponse,
                isLoadingData: false
            }
        case 'GET_DATA_GOALS_FAILED':
            return {
                ...state,
                isLoadingData: false
            }
        case 'GET_GOALS_DETAIL_BY_ID' :
            return {
                ...state,
                isLoadingDetail: true,
            }
        case 'GET_GOALS_DETAIL_BY_ID_SUCCESS' :
            return {
                ...state,
                goalsDataDetail: action.goalDetailResponse,
                isLoadingDetail: false
            }
        case 'POST_GOALS' :
            return {
                ...state,
                modalState: true,
                isLoadingPost: true,
            }
        case 'POST_GOALS_SUCCESS':
            return {
                ...state,
                isLoadingPost: false,
                statusCode: action.message
            }
        case 'POST_GOALS_FAILED':
            return {
                ...state,
                isLoadingPost: false,
                statusCode: action.message
            }
        case 'POST_SUCCESS_OK':
            return {
                ...state,
                modalState : false
            }
        case 'POST_FAILED_OK':
            return {
                ...state,
                modalState : false
            }
        case 'POST_GOAL_PROGRESS' :
            return {
                ...state,
                isLoadingDetail : true
            }
        case 'POST_GOALS_PROGRESS_SUCCESS':
            return {
                ...state,
                isLoadingDetail: false,
            }
        case 'POST_GOALS_PROGRESS_FAILED':
            return {
                ...state,
                isLoadingDetail: false,
                modalStateDetail : true,
            }
        case 'POST_GOALS_PROGRESS_FAILED_OK':
            return {
                ...state,
                modalStateDetail : false,
            }
        case 'EDIT_GOALS' :
            return {
                ...state,
                modalStateEdit: true,
                isLoadingEdit: true
            }
        case 'EDIT_GOALS_SUCCESS' :
            return {
                ...state,
                statusCode: action.message,
                isLoadingEdit: false
            }
        case 'EDIT_GOALS_FAILED' :
            return {
                ...state,
                statusCode: action.message,
                isLoadingEdit: false
            }
        case 'EDIT_SUCCESS_OK' :
            return {
                ...state,
                modalStateEdit : false
            }
        case 'EDIT_FAILED_OK' :
            return {
                ...state,
                modalStateEdit : false
            }
        case 'RESET_STATE':
            return {
                goalsData : [],
                goalsDataDetail: [],
                isLoadingData: false, 
                isLoadingPost: false,
                isLoadingEdit: false,
                isLoadingDetail: false,
                statusCode: null,
                modalState: false,
                modalStateEdit: false,
                modalStateDetail: false,
            }
        default :
            return state;
    }
}

export default goals