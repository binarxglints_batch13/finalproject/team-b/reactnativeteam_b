const initialState = {
    colorState : '#F1F4FA'
}

const colors = (state = initialState, action) => {
    switch(action.type) {
        case 'PINK' :
            return {
                ...state,
                colorState: '#FFBCC2'
            }
        case 'LEMON' :
            return {
                ...state,
                colorState: '#FCF3A1'
            }
        case 'GREEN' :
            return {
                ...state,
                colorState: '#CCF0D7'
            }
        case 'RED' :
            return {
                ...state,
                colorState: '#FF8888'
            }
        case 'PURPLE' :
            return {
                ...state,
                colorState: '#D1CDFA'
            }
        case 'RESET_COLOR' :
            return {
                ...state,
                colorState: '#F1F4FA'
            }
        default: 
            return state
    }
}

export default colors