import axios from "axios"
import { put, takeLatest } from "redux-saga/effects";
import { getHeaders } from "../../Util/authAsyncStorage";


function* getGoals(action) {
    try {
        const headers = yield getHeaders()
        const resDataGoals = yield axios({
            method:'GET',
            url: 'https://remindme.gabatch13.my.id/api/v1/goals',
            headers: headers
        });
        // console.log('res', resDataGoals.data.data)
        yield put({type: 'GET_DATA_GOALS_SUCCESS', dataResponse: resDataGoals.data.data})
    } catch (error) {
        // console.log('error', error.response)
        yield put({type: 'GET_DATA_GOALS_FAILED', message: error.response.status})
    }
}

function* getGoalsDetail(action) {
    try {
        const headers = yield getHeaders()
        const resDataGoalsDetail = yield axios({
            method:'GET',
            url: `https://remindme.gabatch13.my.id/api/v1/goals/${action.goalsId}`,
            headers: headers
        });
        // console.log('res', resDataGoalsDetail.data.data)
        yield put({type:'GET_GOALS_DETAIL_BY_ID_SUCCESS', goalDetailResponse: resDataGoalsDetail.data.data})
    } catch(error) {
        console.log('error', error.response)
    }
}

function* postGoal(action) {
    try {
        // console.log('posting in progress')
        const headers = yield getHeaders()
        const resGoals = yield axios({
            method:'POST',
            url: 'https://remindme.gabatch13.my.id/api/v1/goals',
            data: action.dataGoals,
            headers: headers
        });
        // console.log('res', resGoals)
        yield put({type:'POST_GOALS_SUCCESS', message: resGoals.status})
        yield put({type:'GET_DATA_GOALS'})
    } catch(error) {
        // console.log('err', error.response)
        yield put({type:'POST_GOALS_FAILED', message: error.response.status})
    }
}

function* postProgress(action) {
    try {
        const headers = yield getHeaders()
        const resGoalsProgress = yield axios ({
            method:'POST',
            url: `https://remindme.gabatch13.my.id/api/v1/goals/${action.progressPayload.goalsId}`,
            data: action.progressPayload.progressData,
            headers : headers
        })
        // console.log('post progress sukses', resGoalsProgress)
        yield put({type:'POST_GOALS_PROGRESS_SUCCESS'})
        yield put({type:'GET_GOALS_DETAIL_BY_ID', goalsId: action.progressPayload.goalsId})
        yield put({type:'GET_DATA_GOALS'})
    } catch(error) {
        // console.log("post progree error", error.response)
        yield put({type:'POST_GOALS_PROGRESS_FAILED'})
    }
}

function* updateGoals(action) {
    try {
        const headers = yield getHeaders()
        const resUpdateGoals = yield axios ({
            method:'PUT',
            url: `https://remindme.gabatch13.my.id/api/v1/goals/${action.editPayload.goalsId}`,
            data: action.editPayload.editGoalsData,
            headers: headers
        })
        // console.log("resUpdate", resUpdateGoals)
        yield put({type:'EDIT_GOALS_SUCCESS', message : resUpdateGoals.status})
        yield put({type:'GET_DATA_GOALS'})
    } catch(error) {
        // console.log("update goal error", error.response)
        yield put({type: 'EDIT_GOALS_FAILED', message: error.response.status})
    }
}

export default function* goalsSaga() {
    yield takeLatest('GET_DATA_GOALS', getGoals)
    yield takeLatest('POST_GOALS', postGoal)
    yield takeLatest('GET_GOALS_DETAIL_BY_ID', getGoalsDetail)
    yield takeLatest('POST_GOAL_PROGRESS', postProgress)
    yield takeLatest('EDIT_GOALS', updateGoals)
}