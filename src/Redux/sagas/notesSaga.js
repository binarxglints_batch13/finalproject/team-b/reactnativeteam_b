import { put, takeLatest } from "@redux-saga/core/effects";
import axios from "axios";
import moment from "moment";
import { getHeaders } from "../../Util/authAsyncStorage";

const todayDate = moment.utc().format('YYYY-MM-DD');

function resNote(headers, data) {
    return axios({
        method:'POST',
        url:'https://remindme.gabatch13.my.id/api/v1/notes/',
        data: data,
        headers: {...headers, 'Content-Type': 'multipart/form-data'},
    })
}

function* postNotes(action) {
    try {
        const headers = yield getHeaders();
        const resPostNotes = yield resNote(headers, action.newDataNotes)
        // console.log("resPostNotes",resPostNotes)
        yield put({type:'POST_NOTES_SUCCESS', message: "success"})
        yield put({type: 'GET_NOTES_DATA'})
        yield put({type:'GET_NOTES_BY_DATE', date:todayDate})
    } catch(error) {
        // console.log("error post notes", error.response)
        yield put({type: 'POST_NOTES_FAILED', message: "failed"})
    }
}
function getNotes( headers) {
    return axios({
        method:'GET',
        url : 'https://remindme.gabatch13.my.id/api/v1/notes/',
        headers: headers,
    }).then(res => res.data)
      .catch(error => error)  
}

function* getNotesData(action) {
    try {
        console.log('get data run')
        const headers = yield getHeaders();
        const resNotesData = yield getNotes(headers)
        // console.log("resDataNotes", resNotesData.data)
        yield put({type:'GET_NOTES_DATA_SUCCESS', notesData: resNotesData.data})
        console.log('get dara success')
    } catch(error) {
        // console.log("error get data", error) 
        yield put({type: 'GET_NOTES_DATA_FAILED', })
        console.log('get dara eror')

    }
}

function* getNotesDetail(action) {
    try {
        const headers = yield getHeaders();
        const resNotesDetail = yield axios({
            method : 'GET',
            url: `https://remindme.gabatch13.my.id/api/v1/notes/${action.notesId}`,
            headers : headers
        })
        // console.log("res notes Detail", resNotesDetail)
        yield put({type:'GET_NOTES_DETAIL_SUCCESS', notesDetail})
    } catch(error) {
        // console.log("error notes detail", error.response)
        yield put({type: 'GET_NOTES_DETAIL_FAILED'})
    }
}

function* deleteNotes(action) {
    try {
        const headers = yield getHeaders();
        const resDeleteNotes = yield axios({
            method:'DELETE',
            url: `https://remindme.gabatch13.my.id/api/v1/notes/${action.notesId}`,
            headers: headers
        })
        // console.log("res delete note", resDeleteNotes)
        yield put({type: 'DELETE_NOTES_SUCCESS', message: 'success'})
        yield put({type: 'GET_NOTES_DATA'})
        yield put({type:'GET_NOTES_BY_DATE', date:todayDate})
    } catch(error) {
        // console.log('res error delete note', error.response )
        yield put({type:'DELETE_NOTES_FAILED', message: 'failed'})
    }
}

function* editNotes(action) {
    try {
        const headers = yield getHeaders();
        const resEditNotes = yield axios ({
            method:'PUT',
            url: `https://remindme.gabatch13.my.id/api/v1/notes/${action.editPayload.notesId}`,
            data : action.editPayload.notesData,
            headers : headers
        })
        // console.log('res edit notes', resEditNotes)
        yield put({type: 'EDIT_NOTE_SUCCESS', message: 'success'})
        yield put({type: 'GET_NOTES_DATA'})
        yield put({type:'GET_NOTES_BY_DATE', date:todayDate})
    } catch(error) {
        // console.log('error edit notes', error.response)
        yield put({type: 'EDIT_NOTE_FAILED', message: 'failed'})
    }
}

function* getNotesByDay(action) {
    try {
        const headers = yield getHeaders();
        const resGetData = yield axios ({
            method: 'GET',
            url : `https://remindme.gabatch13.my.id/api/v1/notes/date/${action.date}`,
            headers : headers
        })
        // console.log('res notes date', resGetData)
        yield put({type: 'GET_NOTES_BY_DATE_SUCCESS', notesData: resGetData.data.data})
    } catch(error) {
        // console.log('error get data by date', error.response.data.errors)
        yield put({type: 'GET_NOTES_BY_DATE_FAILED'})
    }
}

export default function* notesSaga() {
    yield takeLatest("POST_NOTES", postNotes)
    yield takeLatest('GET_NOTES_DATA', getNotesData)
    yield takeLatest('GET_NOTES_DETAIL', getNotesDetail)
    yield takeLatest('DELETE_NOTES', deleteNotes)
    yield takeLatest('EDIT_NOTE', editNotes)
    yield takeLatest('GET_NOTES_BY_DATE', getNotesByDay)
}