import {put, takeLatest} from 'redux-saga/effects'
import axios from 'axios';
import { getFCMToken, getHeaders, removeToken, removeUserId, saveToken, saveUserId } from '../../Util/authAsyncStorage';

function* login(action) {
    try {
        // console.log('login running');
        const resLogin = yield axios({
        method: 'POST',
        url: 'https://remindme.gabatch13.my.id/api/v1/auth/signin',
        data: action.dataLogin,
        });
        console.log('res', resLogin)
        yield saveToken(resLogin.data.token);
        yield saveUserId(resLogin.data.data.id);
        yield put({type: 'LOGIN_SUCCESS', message: resLogin.status});
        yield put({type: 'SEND_FCMTOKEN'})
    } catch(error) {
        console.log("err", error.response);
        yield put({type: 'LOGIN_FAILED', 
                      messagePayload: {
                        verifyCheck:error.response.data?.data?.verified ,
                        emailStore: error.response.data?.data?.email,
                        status : error.response.status}});
    }
}

function* logout() {
    try {
      yield removeToken();
      yield removeUserId();
      yield put({type:'LOG_OUT_SUCCESS'})
    } catch (err) {
      console.log(err);
      yield put({type:'LOG_OUT_FAILED'})
    }
  }

function* verify(action) {
  try {
    const resVerify = yield axios({
      method: 'POST',
      url: 'https://remindme.gabatch13.my.id/api/v1/auth/send-verification',
      data : {
        email : action.email
      }
    })
    console.log('resverify', resVerify)
    yield put({type: 'SEND_VERIFY_SUCCESS', messagePayload: {message:resVerify.data.message, status: resVerify.status}})
  } catch(error) {
    console.log('res error verify', error.response)
  }
}

function* forgotPassword(action) {
  try {
    const resForgot = yield axios ({
      method : 'POST',
      url: 'https://remindme.gabatch13.my.id/api/v1/auth/forgot',
      data: {
        email: action.email
      }
    })
    console.log('resForgot', resForgot)
    yield put({type:'FORGOT_SEND_SUCCESS', messagePayload : {
              message : resForgot.data.message,
              status  : resForgot.status
            }})
  } catch(error) {
    console.log('error res forgot', error.response)
    yield put({type: 'FORGOT_SEND_FAILED', messagePayload : {
      message: error.response.data.errors,
      status : error.response.status
    }})

  }
}

function* changePassword(action) {
  try {
    console.log('run change password')
    const resChangePassword = yield axios ({
      method: 'POST',
      url : `https://remindme.gabatch13.my.id/api/v1/auth/forgot/${action.dataPayload.url}`,
      data: action.dataPayload.newPasswordData
    })
    console.log('res change password', resChangePassword)
    console.log('change password sukses')
    yield put({type: 'CHANGE_PASSWORD_AUTH_SUCCESS', message: 'success'})
    console.log('change password selesai')
  } catch(error) {
    console.log('error change password', error.response)
    console.log('change password error')
    yield put({type: 'CHANGE_PASSWORD_AUTH_FAILED', message: 'failed'})
  }
}

function* sendNotificationToken() {
  try {
    const headers = yield getHeaders()
    const FCMToken = yield getFCMToken()
    const resSendNotif = yield axios ({
      method:'POST',
      url : 'https://remindme.gabatch13.my.id/api/v1/notify/subscribe',
      headers : headers,
      data : {
        token: FCMToken
      }
    })
    console.log('res send token', resSendNotif)
  } catch(error) {
    console.log('error send token', error.response)
  }
}

export default function* authSaga() {
    yield takeLatest('LOGIN', login)
    yield takeLatest('LOGOUT', logout)
    yield takeLatest('SEND_VERIFY', verify)
    yield takeLatest('FORGOT_SEND', forgotPassword)
    yield takeLatest('CHANGE_PASSWORD_AUTH', changePassword)
    yield takeLatest('SEND_FCMTOKEN', sendNotificationToken)
}