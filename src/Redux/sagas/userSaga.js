import { put, takeLatest } from "@redux-saga/core/effects";
import axios from "axios";
import { get } from "react-native/Libraries/Utilities/PixelRatio";
import { getHeaders, getToken } from "../../Util/authAsyncStorage";

function* getUserData(action) {
    try {
        const header = yield getHeaders();
        const resUser = yield axios({
            method:'GET',
            url : 'https://remindme.gabatch13.my.id/api/v1/user/getinfo',
            headers: header
        })
        // console.log("resUser", resUser)
        yield put({type: 'GET_USER_DATA_SUCCESS', userData: resUser.data.data})
    } catch(error) {
        console.log("Error", error.response)
    }
}

function* updateProfile(action) {
    try{
        const headers = yield getHeaders();
        const resUpdateProfile = yield axios ({
            method: 'PUT',
            url: 'https://remindme.gabatch13.my.id/api/v1/user',
            headers: {...headers, 'Content-Type': 'multiform/formdata'},
            data: action.newProfile
        })
        console.log('res profile', resUpdateProfile)
        yield put({type:'UPDATE_PROFILE_SUCCESS', message: 'success'})
    } catch(error) {
        console.log('error update profile', error.response)
        yield put({type:'UPDATE_PROFILE_FAILED', message: error.response.data.errors[0]})
    }
}

function* changePassword(action) {
    try {
        const token = yield getToken();
        const resChangePassword = yield axios ({
            method: 'POST',
            url: `https://remindme.gabatch13.my.id/api/v1/user`,
            data : action.newPasswordData
        })
        console.log('res change password', resChangePassword)
        yield put({type: 'CHANGE_PASSWORD_SUCCESS', message: 'success'})
    } catch(error) {
        console.log('error change password', error.response)
        yield put({type: 'CHANGE_PASSWORD_FAILED', message: 'failed'})
    }
}

export default function* userSaga() {
    yield takeLatest("GET_USER_DATA", getUserData)
    yield takeLatest('UPDATE_PROFILE', updateProfile)
    yield takeLatest('CHANGE_PASSWORD', changePassword)
}