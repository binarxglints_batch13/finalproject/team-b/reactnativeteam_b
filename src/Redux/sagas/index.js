import { all } from "@redux-saga/core/effects";
import signupSaga from './SignUpSaga';
import authSaga from "./authSaga";
import goalsSaga from "./goalsSaga";
import userSaga from "./userSaga";
import notesSaga from "./notesSaga";

export default function* rootSagas() {
    yield all([signupSaga(), authSaga(), goalsSaga(), userSaga(), notesSaga() ])
}