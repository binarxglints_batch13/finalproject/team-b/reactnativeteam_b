import {call, put, takeLatest} from 'redux-saga/effects';
import axios from 'axios';

function registerFetch(data) {
    return axios({
        method: 'POST',
        url: 'https://remindme.gabatch13.my.id/api/v1/auth/signup',
        data: data,
    })
    .then(response => ({response}))
    .catch(error => ({error:error.response}))
}

function* register(action) {
    try {
      const resLogin = yield axios({
        method: 'POST',
        url: 'https://remindme.gabatch13.my.id/api/v1/auth/signup',
        data: action.signupData,
      });
      console.log('res', resLogin)
      yield put({type: 'REGISTER_SUCCESS', status: resLogin.status});
    } catch (error) {
      console.log('ini error', error.response)
      yield put({type: 'REGISTER_FAILED', messagePayload: {status:error.response.status, message: error.response.data.errors}});
    }
  }

  function* verify(action) {
    try {
      const resVerify = yield axios({
        method: 'POST',
        url: 'https://remindme.gabatch13.my.id/api/v1/auth/send-verification',
        data : {
          email : action.email
        }
      })
      console.log('resverify', resVerify)
      yield put({type: 'SEND_VERIFY_SUCCESS', messagePayload: {message:resVerify.data.message, status: resVerify.status}})
    } catch(error) {
      console.log('res error verify', error.response)
    }
  }

export default function* signupSaga() {
    yield takeLatest('REGISTER', register);
    yield takeLatest('SEND_VERIFY', verify)
  }